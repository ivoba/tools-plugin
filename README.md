OXID6 The toolkit-core-classes with useful functions for other modules
======

![vendor-logo the-real-world.de](picture.png)

### Features

* API-Calls
* String Manipulation
* File Transfer
* XML processing
* Monolog-Logger
* direct DB processing
* connect to OXID Config

### Module installation via composer

In order to install the module via composer run one of the following commands in commandline in your shop base directory
(where the shop's composer.json file resides).
* **composer require therealworld/tools-plugin** to install the actual version compatible with OXID6

## Bugs and Issues

If you experience any bugs or issues, please report them in on https://bitbucket.org/therealworld/tools-plugin/issues.

