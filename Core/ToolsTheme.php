<?php

/**
* @author    Mario Lorenz, www.the-real-world.de
* @copyright 2021 the-real-world.de
* @license   https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

namespace TheRealWorld\ToolsPlugin\Core;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\DatabaseProvider;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;
use TheRealWorld\ToolsPlugin\Core\ToolsConfig;

class ToolsTheme
{
    /**
    * Config Keys that must be inside of a ConfigTransfer-Array
    * @param array
    */
    protected static $_aConfigTransferKeys = [
        'type', 'value', 'group', 'constraints', 'pos'
    ];

    /**
    * Get the List of themes
    *
    * @param array $aExcludeConfigFields
    * @param int   $iShopId - ShopId
    *
    * @return array
    */
    public static function getAllThemeOptions($aExcludeConfigFields = [], $iShopId = null)
    {
        $oConfig = Registry::getConfig();

        $iShopId = $iShopId ?? $oConfig->getShopId();

        $oConfig->initVars($iShopId);

        if (!$sThemeForShopId = $oConfig->getConfigParam('sCustomTheme')) {
            $sThemeForShopId = $oConfig->getConfigParam('sTheme');
        }

        $oTheme = oxNew(\OxidEsales\Eshop\Core\Theme::class);
        $aAllThemeOptions = [];

        foreach ($oTheme->getList() as $oThemeElement) {
            $sThemeId = $oThemeElement->getInfo('id');
            $aThemeOptions = [
                'id'          => $sThemeId,
                'title'       => $oThemeElement->getInfo('title'),
                'description' => $oThemeElement->getInfo('description'),
                'thumbnail'   => $oThemeElement->getInfo('thumbnail'),
                'version'     => $oThemeElement->getInfo('version'),
                'author'      => $oThemeElement->getInfo('author'),
                'active'      => $sThemeForShopId == $sThemeId
            ];
            if ($oThemeElement->getInfo('parentTheme')) {
                $aThemeOptions['parenttheme'] = $oThemeElement->getInfo('parentTheme');
                $aThemeOptions['parentversions'] = $oThemeElement->getInfo('parentVersions');
            }

            $aThemeSettings = ToolsConfig::getShopConfigValuesForExport([], $iShopId, 'theme:' . $sThemeId);

            foreach ($aThemeSettings as $sSettingsName => $aSetting) {
                // dont export this option
                if (in_array($sSettingsName, $aExcludeConfigFields)) {
                    continue;
                }
                $aThemeOptions['themeSettings'][$sSettingsName] = $aSetting;
                if (empty($aSetting['constraints'])) {
                    unset($aThemeOptions['themeSettings'][$sSettingsName]['constraints']);
                }
            }
            $aAllThemeOptions[$sThemeId] = $aThemeOptions;
        }
        return $aAllThemeOptions;
    }

    /**
    * Get the Themeoptions as Array
    *
    * @param string - $sThemeId
    *
    * @return array
    */
    public static function getThemeOptions($sThemeId = '', $iShopId = null)
    {
        $mResult = false;

        $oConfig = Registry::getConfig();

        $iShopId = $iShopId ?? $oConfig->getShopId();

        $oConfig->initVars($iShopId);

        if ($sThemeId) {
            $mResult = ToolsConfig::getShopConfigValuesForExport([], $iShopId, 'theme:' . $sThemeId);
        }

        return $mResult;
    }

    /**
    * Set Themeoptions for a target theme
    *
    * @param string - $sThemeId - Id of the Theme
    * @param array  - $aThemeOptions - Array with ThemeOptions
    *
    * @return boolean
    */
    public static function setThemeOptions($sThemeId = '', $aThemeOptions = [])
    {
        $bResult = false;
        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $oConfig = Registry::getConfig();
        $sShopId = $oConfig->getBaseShopId();

        $oTheme = oxNew(\OxidEsales\Eshop\Core\Theme::class);
        $oTheme->load($sThemeId);

        if (is_array($aThemeOptions) && count($aThemeOptions) && $oTheme->getId()) {
            foreach ($aThemeOptions as $sConfigName => $aConfig) {
                // check if ConfigTransfer-Array are complete
                foreach (self::$_aConfigTransferKeys as $sConfigKey) {
                    if (!array_key_exists($sConfigKey, $aConfig)) {
                        $aConfig[$sConfigKey] = null;
                    }
                }

                // check for array-Variables
                if (in_array($aConfig['type'], ['arr', 'aarr']) && is_array($aConfig['value'])) {
                    $aConfig['value'] = serialize($aConfig['value']);
                }

                $bResult = true;
                // check if option exists
                if (
                    !$sOxId = ToolsDB::getAnyId(
                        'oxconfig',
                        [
                            'oxvarname' => $sConfigName,
                            'oxmodule'  => 'theme:' . $sThemeId
                        ]
                    )
                ) {
                    $sOxId = md5($sThemeId . "." . $sConfigName);

                    $sSql = "replace into `oxconfig`
                        set `oxid` = " . $oDb->quote($sOxId) . ",
                            `oxshopid` = " . $oDb->quote($sShopId) . ",
                            `oxmodule` = " . $oDb->quote('theme:' . $sThemeId) . ",
                            `oxvarname` = " . $oDb->quote($sConfigName) . ",
                            `oxvartype` = " . $oDb->quote($aConfig['type']) . ",
                            `oxvarvalue` = encode( " . $oDb->quote($aConfig['value']) . ", " . $oDb->quote($oConfig->getConfigParam('sConfigKey')) . ")";
                    ToolsDB::execute($sSql);

                    // display
                    $sSql = "replace into `oxconfigdisplay`
                        set `oxid` = " . $oDb->quote($sOxId) . ",
                            `oxcfgmodule` = " . $oDb->quote('theme:' . $sThemeId) . ",
                            `oxcfgvarname` = " . $oDb->quote($sConfigName) . ",
                            `oxgrouping` = " . $oDb->quote($aConfig['group']) . ",
                            `oxvarconstraint` = " . $oDb->quote($aConfig['constraints']) . ",
                            `oxpos` = " . $oDb->quote($aConfig['pos']);
                    ToolsDB::execute($sSql);
                }
            }
        }
        return $bResult;
    }

    /**
    * delete all or a special theme option
    *
    * @param string - $sThemeId - Id of the Theme
    * @param array  - $aThemeOptions - Array with ThemeOptions (if empty all options would be delete)
    *
    * @return boolean
    */
    public static function deleteThemeOptions($sThemeId = '', $aThemeOptions = null)
    {
        $bResult = false;
        $oTheme = oxNew(\OxidEsales\Eshop\Core\Theme::class);
        $oTheme->load($sThemeId);

        if ($oTheme->getId()) {
            $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
            $sShopId = Registry::getConfig()->getBaseShopId();

            // delete existing values for theme
            if (is_array($aThemeOptions)) {
                foreach ($aThemeOptions as $sOptionName) {
                    $sSql = "delete from `oxconfig`
                        where `oxshopid` = " . $oDb->quote($sShopId) . "
                        and `oxmodule` = " . $oDb->quote('theme:' . $sThemeId) . "
                        and `oxvarname` = " . $oDb->quote($sOptionName);
                    ToolsDB::execute($sSql);

                    $sSql = "delete from `oxconfigdisplay`
                        where `oxcfgmodule` = " . $oDb->quote('theme:' . $sThemeId) . "
                        and `oxcfgvarname` = " . $oDb->quote($sOptionName);
                    ToolsDB::execute($sSql);
                }
            }
            // delete all theme options
            else {
                $sSql = "delete from `oxconfig`
                    where `oxshopid` = " . $oDb->quote($sShopId) . "
                    and `oxmodule` = " . $oDb->quote('theme:' . $sThemeId);
                ToolsDB::execute($sSql);

                $sSql = "delete from `oxconfigdisplay`
                    where `oxcfgmodule` = " . $oDb->quote('theme:' . $sThemeId);
                ToolsDB::execute($sSql);
            }

            $bResult = true;
        }
        return $bResult;
    }

    /**
    * get Option Groups from theme
    *
    * @param string - $sThemeId - Id of the Theme
    *
    * @return mixed (boolean / array)
    */
    public static function getOptionGroups($sThemeId = '')
    {
        $mResult = false;
        $oTheme = oxNew(\OxidEsales\Eshop\Core\Theme::class);
        $oTheme->load($sThemeId);

        if ($oTheme->getId()) {
            $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
            $sSelect = "select `oxgrouping`
                from `oxconfigdisplay`
                where `oxcfgmodule` = " . $oDb->quote('theme:' . $sThemeId) . "
                group by `oxgrouping`
                order by `oxgrouping`";

            $oResults = $oDb->select($sSelect);
            if ($oResults != false && $oResults->count() > 0) {
                $mResult = [];

                while (!$oResults->EOF) {
                    $aRow = $oResults->getFields();
                    $mResult[] = $aRow;
                    $oResults->fetchRow();
                }
            }
        }
        return $mResult;
    }
}
