<?php

/**
* @author    Mario Lorenz, www.the-real-world.de
* @copyright 2021 the-real-world.de
* @license   https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

namespace TheRealWorld\ToolsPlugin\Core;

use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\ToolsPlugin\Core\ToolsLog;
use TheRealWorld\ToolsPlugin\Core\ToolsLang;

class ToolsFile
{
    /**
    * array with default excluded files for folderscans
    * @param array
    */
    protected static $_aDefaultExcludedFiles = [
        ".",
        "..",
        ".htaccess"
    ];

    /**
    * determines the files in a directory
    * If a filter is defined, then only the filtered files come into the result array
    * A filter can be equipped with an exclude filter.
    *
    * @param string $sPath          scanned path
    * @param string $sFilterRegex   regex for filter the file
    * @param boolean $bObserveFileChange - chould we observe the last change of the file
    *
    * @return mixed, normally array with filenames
    *
    */
    public static function getFileNamesFromPath($sPath = '', $sFilterRegex = '', $bObserveFileChange = false)
    {
        $mResult = false;

        $iFileAgeForImport = (int)Registry::getConfig()->getConfigParam('iTRWToolsFileAgeForImport');

        if ($sPath && is_dir($sPath)) {
            $aEntries = scandir($sPath);
            $aFiles = [];
            foreach ($aEntries as $sEntry) {
                if (
                    in_array($sEntry, self::$_aDefaultExcludedFiles) ||
                    (is_file($sPath . $sEntry) && $bObserveFileChange === true && filemtime($sPath . $sEntry) > (time() - $iFileAgeForImport)) ||
                    is_dir($sPath . $sEntry)
                ) {
                    continue;
                }

                // regex Filter
                if (!empty($sFilterRegex) && (preg_match($sFilterRegex, $sEntry) !== 1)) {
                    continue;
                }

                $aFiles[] = $sEntry;
            }
            if (count($aFiles)) {
                $mResult = $aFiles;
                ToolsLog::setLogEntry(
                    sprintf(
                        ToolsLang::translateString('found follow file(s): %s', 'TRWTOOLSPLUGIN'),
                        implode(', ', $aFiles)
                    ),
                    __CLASS__ . ' - ' . __FUNCTION__
                );
            }
        } else {
            ToolsLog::setLogEntry(
                sprintf(
                    ToolsLang::translateString('Path not found: %s', 'TRWTOOLSPLUGIN'),
                    $sPath
                ),
                __CLASS__ . ' - ' . __FUNCTION__,
                'error'
            );
        }
        return $mResult;
    }

    /**
    * move files on the server
    * If a filter is defined, then only the filtered files are moved
    *
    * @param string $sPathSource   Source Path
    * @param string $sPathTarget   Target Path
    * @param string $sFilterRegex   regex for filter the file
    * @param boolean $bObserveFileChange - should we observe the last change of the file
    * @param boolean $bIsArchive - if true a timestamp would be add
    *
    * @return array with filenames
    *
    */
    public static function moveFiles($sPathSource = '', $sPathTarget = '', $sFilterRegex = '', $bObserveFileChange = false, $bIsArchive = false)
    {
        $bResult = false;

        if (!$sPathSource) {
            ToolsLog::setLogEntry(
                sprintf(
                    ToolsLang::translateString('Path not defined', 'TRWTOOLSPLUGIN'),
                    'sPathSource'
                ),
                __CLASS__ . ' - ' . __FUNCTION__,
                'error'
            );
            return $bResult;
        }

        if (!$sPathTarget) {
            ToolsLog::setLogEntry(
                sprintf(
                    ToolsLang::translateString('Path not defined', 'TRWTOOLSPLUGIN'),
                    'sPathTarget'
                ),
                __CLASS__ . ' - ' . __FUNCTION__,
                'error'
            );
            return $bResult;
        }

        if (!$bResult = self::checkPathPermissions($sPathSource, false, true, false, false)) {
            return $bResult;
        }

        if (!$bResult = self::checkPathPermissions($sPathTarget, false, false, true, false)) {
            return $bResult;
        }

        if ($aFiles = self::getFileNamesFromPath($sPathSource, $sFilterRegex, $bObserveFileChange)) {
            $bResult = true;
            foreach ($aFiles as $sFile) {
                $sTargetFile = ($bIsArchive ? self::getBackupName($sFile, '_archive_', true) : $sFile);
                if (
                    rename(
                        $sPathSource . $sFile,
                        $sPathTarget . $sTargetFile
                    )
                ) {
                    ToolsLog::setLogEntry(
                        sprintf(
                            ToolsLang::translateString($bIsArchive ? 'Backup created: %s -> %s' : 'File moved: %s -> %s', 'TRWTOOLSPLUGIN'),
                            $sFile,
                            $sTargetFile
                        ),
                        __CLASS__ . ' - ' . __FUNCTION__
                    );
                } else {
                    ToolsLog::setLogEntry(
                        sprintf(
                            ToolsLang::translateString('Failed to move or rename the file: %s -> %s', 'TRWTOOLSPLUGIN'),
                            $sFile,
                            $sTargetFile
                        ),
                        __CLASS__ . ' - ' . __FUNCTION__,
                        'error'
                    );
                }
            }
        }
        return $bResult;
    }

    /**
    * move paths on the server
    *
    * @param string $sPathSource   Source Path
    * @param string $sPathTarget   Target Path
    * @param boolean $bIsArchive - if true a timestamp would be add
    *
    * @return array with filenames
    *
    */
    public static function movePath($sPathSource = '', $sPathTarget = '', $bIsArchive = false)
    {
        $bResult = false;

        if (!$sPathSource) {
            ToolsLog::setLogEntry(
                sprintf(
                    ToolsLang::translateString('Path not defined', 'TRWTOOLSPLUGIN'),
                    'sPathSource'
                ),
                __CLASS__ . ' - ' . __FUNCTION__,
                'error'
            );
            return $bResult;
        }

        if (!$sPathTarget) {
            ToolsLog::setLogEntry(
                sprintf(
                    ToolsLang::translateString('Path not defined', 'TRWTOOLSPLUGIN'),
                    'sPathTarget'
                ),
                __CLASS__ . ' - ' . __FUNCTION__,
                'error'
            );
            return $bResult;
        }

        $sPathTarget = ($bIsArchive ? self::getBackupName($sPathTarget, '_archive_') : $sPathTarget);

        if ($bIsArchive) {
            ToolsLog::setLogEntry(
                sprintf(
                    ToolsLang::translateString('Backup created: %s -> %s', 'TRWTOOLSPLUGIN'),
                    "\n&nbsp;&nbsp;" . $sPathSource,
                    "\n&nbsp;&nbsp;" . $sPathTarget
                ),
                __CLASS__ . ' - ' . __FUNCTION__
            );
        }

        if (!$bResult = self::checkPathPermissions($sPathSource, false, true, true, false)) {
            return $bResult;
        }

        $bResult = rename($sPathSource, $sPathTarget);
        return $bResult;
    }

    /**
    * move and extract a Zipcompressed file
    *
    * @param string $sSourceName   the name of the sourcefile
    * @param string $sSourceExt    the extension of the sourcefile
    * @param string $sPathSource   the path of the sourcefile
    * @param string $sPathTarget   the targetpath of the extraction
    * @param string $sPathReady    the path for the archive of the extracted files
    * @param boolean $bObserveFileChange - chould we observe the last change of the file
    *
    * @return int Resultcode as Binary-Code: 1 = file are extract, 2 readyfile are moved to archive, 4 = targetpath moved to archive
    */
    public static function moveAndExtractFile($sSourceName = '', $sSourceExt = 'zip', $sPathSource = '', $sPathTarget = '', $sPathReady = '', $bObserveFileChange = false)
    {
        $iResult = 0;

        $sSourceFile = $sSourceName . '.' . $sSourceExt;
        $sPathExtractTarget = $sPathTarget . $sSourceName;

        if (file_exists($sPathSource . $sSourceFile)) {
            // create a possible backup
            if (
                self::movePath(
                    $sPathExtractTarget,
                    $sPathExtractTarget,
                    true
                )
            ) {
                $iResult |= 4;
            }

            // extract the new file
            if (
                self::extractZipFile(
                    $sPathSource,
                    $sSourceFile,
                    $sPathExtractTarget,
                    $bObserveFileChange
                )
            ) {
                $iResult |= 1;
            }

            // move the extracted file
            if (
                self::moveFiles(
                    $sPathSource,
                    $sPathReady,
                    '/^' . $sSourceFile . '$/',
                    false,
                    true
                )
            ) {
                $iResult |= 2;
            }
        }
        return $iResult;
    }

    /**
    * GZIPs a file on disk (appending .gz to the name)
    *
    * @param string $sFileSource  Source File
    * @param string $sPathSource  Source Path
    * @param string $sFileTarget  optional Target File
    * @param string $sPathTarget  optional Target Path
    * @param integer $iLevel GZIP compression level (default: 9)

    * @return boolean
    */
    public static function compressGZFile($sFileSource = '', $sPathSource = '', $sFileTarget = '', $sPathTarget = '', $iLevel = 9)
    {
        $bResult = false;
        $bError = false;

        if (!$sFileTarget) {
            $sFileTarget = $sFileSource;
        }
        if (!$sPathTarget) {
            $sPathTarget = $sPathSource;
        }
        $sFileTarget .= '.gz';

        if (!is_dir($sPathSource)) {
            ToolsLog::setLogEntry(
                sprintf(
                    ToolsLang::translateString('Path not found: %s', 'TRWTOOLSPLUGIN'),
                    $sPathSource
                ),
                __CLASS__ . ' - ' . __FUNCTION__,
                'error'
            );
            $bError = true;
            ;
        }

        if (!is_dir($sPathTarget)) {
            ToolsLog::setLogEntry(
                sprintf(
                    ToolsLang::translateString('Path not found: %s', 'TRWTOOLSPLUGIN'),
                    $sPathTarget
                ),
                __CLASS__ . ' - ' . __FUNCTION__,
                'error'
            );
            $bError = true;
            ;
        }

        if (!is_file($sPathSource . $sFileSource)) {
            ToolsLog::setLogEntry(
                sprintf(
                    ToolsLang::translateString('File not found: %s', 'TRWTOOLSPLUGIN'),
                    $sPathSource . $sFileSource
                ),
                __CLASS__ . ' - ' . __FUNCTION__,
                'error'
            );
            $bError = true;
        }

        if (!$bError) {
            if ($oOut = gzopen($sPathTarget . $sFileTarget, 'wb' . $iLevel)) {
                if ($oIn = fopen($sPathSource . $sFileSource, 'rb')) {
                    while (!feof($oIn)) {
                        gzwrite($oOut, fread($oIn, 1024 * 512));
                    }
                    fclose($oIn);
                    $bResult = true;
                } else {
                    ToolsLog::setLogEntry(
                        sprintf(
                            ToolsLang::translateString('Could not open file: %s', 'TRWTOOLSPLUGIN'),
                            $sPathSource . $sFileSource
                        ),
                        __CLASS__ . ' - ' . __FUNCTION__,
                        'error'
                    );
                }
                gzclose($oOut);
            } else {
                ToolsLog::setLogEntry(
                    sprintf(
                        ToolsLang::translateString('Could not open file: %s', 'TRWTOOLSPLUGIN'),
                        $sPathTarget . $sFileTarget
                    ),
                    __CLASS__ . ' - ' . __FUNCTION__,
                    'error'
                );
            }
        }
        return $bResult;
    }

    /**
    * extract a zip-File
    *
    * @param string $sPathSource  Source Path
    * @param string $sFileName    Filename
    * @param string $sPathTarget  Target Path
    * @param boolean $bObserveFileChange - chould we observe the last change of the file
    *
    * @return boolean
    */
    public static function extractZipFile($sPathSource = '', $sFileName = '', $sPathTarget = '', $bObserveFileChange = false)
    {
        $bResult = false;
        $iFileAgeForImport = (int)Registry::getConfig()->getConfigParam('iTRWToolsFileAgeForImport');
        if (
            is_file($sPathSource . $sFileName) &&
            (
                (
                    $bObserveFileChange === true &&
                    filemtime($sPathSource . $sFileName) > ( time() - $iFileAgeForImport )
                ) ||
                $bObserveFileChange === false
            )
        ) {
            if ($oZip = new \ZipArchive()) {
                try {
                    $oZip->open($sPathSource . $sFileName);
                    ToolsLog::setLogEntry(
                        sprintf(
                            ToolsLang::translateString('extract: %s', 'TRWTOOLSPLUGIN'),
                            $sPathSource . $sFileName
                        ),
                        __CLASS__ . ' - ' . __FUNCTION__,
                        'info'
                    );

                    if ($sPathTarget && !file_exists($sPathTarget)) {
                        mkdir($sPathTarget);
                    }

                    $oZip->extractTo($sPathTarget ? $sPathTarget : $sPathSource);
                    $oZip->close();
                    $bResult = true;
                } catch (\Exception $e) {
                    $sMessage = $e->getMessage();
                    ToolsLog::setLogEntry(
                        $sMessage,
                        __CLASS__ . ' - ' . __FUNCTION__,
                        'error'
                    );
                }
            }
        }
        return $bResult;
    }

    /**
    * return a Backupname of a file or a path with timestamp
    *
    * @param string $sString - the file- or pathname
    * @param string $sAddString - the part in the Backupfilename between the originalname and the timestamp
    * @param booleat $bIsFile - is it a file? If true we consider the fileextension
    *
    * @return string
    */
    public static function getBackupName($sString = '', $sAddString = '_backup_', $bIsFile = false)
    {
        if ($sString) {
            $sBackupName = '';
            $sAddTimeStamp = date('YmdHis');
            if ($bIsFile && ($sPosFileExtension = strripos($sString, '.')) !== false) {
                $sBackupName .= substr($sString, 0, $sPosFileExtension);
                $sBackupName .= $sAddString;
                $sBackupName .= $sAddTimeStamp;
                $sBackupName .= substr($sString, $sPosFileExtension);
            } else {
                $sBackupName .= $sString;
                $sBackupName .= $sAddString;
                $sBackupName .= $sAddTimeStamp;
            }
        }
        return $sBackupName;
    }

    /**
    * Delete files Recursively
    *
    * @param string $sPath - Filepath
    *
    * @return null
    */
    public static function deleteRecursively($sPath = '', $bDeleteRootPath = true)
    {
        if (is_dir($sPath)) {
            $aFiles = scandir($sPath);
            foreach ($aFiles as $sFile) {
                if (!in_array($sFile, self::$_aDefaultExcludedFiles)) {
                    if (filetype($sPath . $sFile) == 'dir') {
                        self::deleteRecursively($sPath . $sFile . '/', true);
                    } else {
                        self::deleteFile($sPath . $sFile);
                    }
                }
            }
            reset($aFiles);
            if ($bDeleteRootPath) {
                rmdir($sPath);
            }
        }
    }

    /**
    * Removes files from path
    *
    * @param string $sPath
    *
    * @return null
    */
    public static function deleteFilesFromPath($sPath = '')
    {
        if (is_dir($sPath)) {
            $aFiles = glob($sPath . '*');
            if (is_array($aFiles)) {
                // delete all template cache files
                foreach ($aFiles as $sFile) {
                    self::deleteFile($sFile);
                }
            }
        }
    }

    /**
    * delete a File
    *
    * @param string - $sFile - The Path + Filename
    *
    * @return null
    */
    public static function deleteFile($sFile = '')
    {
        if ($sFile && is_file($sFile) && file_exists($sFile)) {
            @unlink($sFile);
        }
    }

    /**
    * check a path and create it, if its not exists
    *
    * @param string - $sPath - The Path + Filename
    * @param boolean - $bRelative - is the path relative to the shoproot?
    *
    * @return boolean
    */
    public static function createPath($sPath = '', $bRelative = true)
    {
        $bResult = false;
        if ($sPath) {
            $sFullPath = ($bRelative ? Registry::getConfig()->getConfigParam('sShopDir') : '');
            $sFullPath .= $sPath;
            if (file_exists($sFullPath) && is_dir($sFullPath)) {
                $bResult = true;
            } else {
                if (!$bResult = @mkdir($sFullPath, 0755, true)) {
                    ToolsLog::setLogEntry(
                        sprintf(
                            ToolsLang::translateString('Cant create path: %s', 'TRWTOOLSPLUGIN'),
                            $sFullPath
                        ),
                        __CLASS__ . ' - ' . __FUNCTION__
                    );
                }
            }
        }
        return $bResult;
    }

    /**
    * check the permissions of a path
    *
    * @param string - $sPath - The Path
    * @param boolean - $bRelative - is the path relative to the shoproot?
    * @param boolean - $bReadable - check readable
    * @param boolean - $bWritable - check writable
    * @param boolean - $bCreatePath - create a path if it doesnt exists?
    *
    * @return null
    */
    public static function checkPathPermissions($sPath, $bRelative = true, $bReadable = true, $bWritable = true, $bCreatePath = true)
    {
        $bResult = true;
        if ($sPath) {
            $sFullPath = ($bRelative ? Registry::getConfig()->getConfigParam('sShopDir') : '');
            $sFullPath .= $sPath;
            if (!$bPathExists = (file_exists($sFullPath) && is_dir($sFullPath))) {
                ToolsLog::setLogEntry(
                    sprintf(
                        ToolsLang::translateString('Path not found: %s', 'TRWTOOLSPLUGIN'),
                        $sFullPath
                    ),
                    __CLASS__ . ' - ' . __FUNCTION__,
                    'error'
                );
                $bPathExists = self::createPath($sPath, $bRelative);
            }

            if ($bPathExists) {
                if ($bReadable && !is_readable($sFullPath)) {
                    ToolsLog::setLogEntry(
                        sprintf(
                            ToolsLang::translateString('Path not readable: %s', 'TRWTOOLSPLUGIN'),
                            $sFullPath
                        ),
                        __CLASS__ . ' - ' . __FUNCTION__,
                        'error'
                    );
                    $bResult = false;
                }
                if ($bWritable && !is_writable($sFullPath)) {
                    ToolsLog::setLogEntry(
                        sprintf(
                            ToolsLang::translateString('Pfad not writable: %s', 'TRWTOOLSPLUGIN'),
                            $sFullPath
                        ),
                        __CLASS__ . ' - ' . __FUNCTION__,
                        'error'
                    );
                    $bResult = false;
                }
            }
        } else {
            ToolsLog::setLogEntry(
                sprintf(
                    ToolsLang::translateString('Path not defined', 'TRWTOOLSPLUGIN'),
                    $sPath
                ),
                __CLASS__ . ' - ' . __FUNCTION__,
                'error'
            );
            $bResult = false;
        }
        return $bResult;
    }

    /**
    * delete all Images of Article
    *
    * @param \OxidEsales\Eshop\Application\Model\Article $oArticle given article
    * @param int - $iMaxImage - The max count of masterpictures
    *
    * @return null
    */
    public static function deleteAllImagesOfArticle(\OxidEsales\Eshop\Application\Model\Article $oArticle, $iMaxImage)
    {
        $oPicHandler = Registry::getPictureHandler();
        if (!$oArticle->isDerived()) {
            // Icon
            if ($oArticle->oxarticles__oxicon->value) {
                $oPicHandler->deleteMainIcon($oArticle);
                $oArticle->oxarticles__oxicon = new \OxidEsales\Eshop\Core\Field();
            }

            // Thumbnail
            if ($oArticle->oxarticles__oxthumb->value) {
                $oPicHandler->deleteThumbnail($oArticle);
                $oArticle->oxarticles__oxthumb = new \OxidEsales\Eshop\Core\Field();
            }

            // MasterPictures
            if ((int)$iMaxImage) {
                for ($iIndex = 1; $iIndex <= $iMaxImage; $iIndex++) {
                    if ($oArticle->{"oxarticles__oxpic" . $iIndex}->value) {
                        $oPicHandler->deleteArticleMasterPicture($oArticle, $iIndex, true);
                        $oArticle->{"oxarticles__oxpic" . $iIndex} = new \OxidEsales\Eshop\Core\Field();
                    }
                    if (isset($oArticle->{"oxarticles__oxzoom" . $iIndex})) {
                        $oArticle->{"oxarticles__oxzoom" . $iIndex} = new \OxidEsales\Eshop\Core\Field();
                    }
                }
            }
        }
    }

    /**
    * create a relative Path from one Url to another
    *
    * @param string - $sFrom - The source Url
    * @param string - $sTo - The target Url
    *
    * @return null
    */
    public static function getRelativePath($sFrom, $sTo)
    {
        // some compatibility fixes for Windows paths
        $sFrom = is_dir($sFrom) ? rtrim($sFrom, '\/') . '/' : $sFrom;
        $sFrom = str_replace('\\', '/', $sFrom);
        $sTo = is_dir($sTo) ? rtrim($sTo, '\/') . '/' : $sTo;
        $sTo = str_replace('\\', '/', $sTo);

        $aFrom = explode('/', $sFrom);
        $aTo   = explode('/', $sTo);
        $aRelativePath = $aTo;

        foreach ($aFrom as $iDepth => $sDir) {
            // find first non-matching dir
            if ($sDir === $aTo[$iDepth]) {
                // ignore this directory
                array_shift($aRelativePath);
            } else {
                // get number of remaining dirs to $aFrom
                $iRemaining = count($aFrom) - $iDepth;
                if ($iRemaining > 1) {
                    // add traversals up to first matching dir
                    $iPadLength = (count($aRelativePath) + $iRemaining - 1) * -1;
                    $aRelativePath = array_pad($aRelativePath, $iPadLength, '..');
                    break;
                } else {
                    $aRelativePath[0] = './' . $aRelativePath[0];
                }
            }
        }
        return implode('/', $aRelativePath);
    }
}
