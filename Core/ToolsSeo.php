<?php

/**
* @author    Mario Lorenz, www.the-real-world.de
* @copyright 2021 the-real-world.de
* @license   https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

namespace TheRealWorld\ToolsPlugin\Core;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;
use OxidEsales\Eshop\Core\DatabaseProvider;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;

class ToolsSeo
{
    /**
    * create a static Url
    *
    * @param string $sStdUrl - the standard url e.g. index.php?cl=xyz
    * @param array  $aSeoUrl - array with all static seo-urls, keys = langabbr e.g. ['de' => 'xyz/', 'en' => 'en/xyz/']
    *
    * @return string - objId of Seo-Url
    */
    public static function setStaticUrl($sStdUrl, $aSeoUrl)
    {
        $sResult = '';
        if (!$sObjectId = self::_getIdForStaticUrl($sStdUrl)) {
            $aStaticUrl = [
                'oxseo__oxstdurl' => $sStdUrl
            ];

            $oLang = Registry::getLang();
            foreach ($oLang->getLanguageIds() as $iLang => $sLangAbbr) {
                if (array_key_exists($sLangAbbr, $aSeoUrl)) {
                    $aStaticUrl['oxseo__oxseourl'][$iLang] = $aSeoUrl[$sLangAbbr];
                }
            }

            if (isset($aStaticUrl['oxseo__oxseourl'])) {
                $sResult = Registry::getSeoEncoder()->encodeStaticUrls(
                    $aStaticUrl,
                    Registry::getConfig()->getShopId(),
                    $oLang->getEditLanguage()
                );
            }
        }
        return $sResult;
    }

    /**
    * delete a static Url
    *
    * @param string $sStdUrl - the standard url e.g. index.php?cl=xyz
    *
    * @return boolean
    */
    public static function deleteStaticUrl($sStdUrl)
    {
        $iShopId = Registry::getConfig()->getShopId();

        foreach (Registry::getLang()->getLanguageIds() as $iLang => $sLangAbbr) {
            $sObjectId = self::_getIdForStaticUrl(
                $sStdUrl,
                $iLang,
                $iShopId
            );

            $bResult = Registry::getSeoEncoder()->deleteSeoEntry(
                $sObjectId,
                $iShopId,
                $iLang,
                'static'
            );
        }
        return $bResult;
    }

    /**
    * update the Seo Meta Data
    *
    * @param string $sOxId   OxId
    * @param string $sOxId   Keywords
    * @param string $sOxId   Descriptions
    * @param int    $iLang   active language (optional). default null
    * @param int    $iShopId active shop id (optional). default null
    *
    * @return boolean
    */
    public static function updateSeoMeta($sOxId, $sKeywords, $sDescription, $iLang = null, $iShopId = null)
    {
        $oStr = Str::getStr();
        $oDb = DatabaseProvider::getDb();

        if (is_null($iShopId)) {
            $iShopId = Registry::getConfig()->getShopId();
        }
        if (is_null($iLang)) {
            $iLang = Registry::getLang()->getBaseLanguage();
        }

        if ($sKeywords !== false) {
            $sKeywords = $oStr->htmlspecialchars(
                Registry::getSeoEncoder()->encodeString($oStr->strip_tags($sKeywords), false, $iLang)
            );
        }

        if ($sDescription !== false) {
            $sDescription = $oStr->htmlspecialchars(
                $oStr->strip_tags($sDescription)
            );
        }

        $sObj2SeoDataTable = 'oxobject2seodata';
        $sSeoQuery = "insert into {$sObj2SeoDataTable}
            (`oxobjectid`, `oxshopid`, `oxlang`, `oxkeywords`, `oxdescription`)
            values
            (
                " . $oDb->quote($sOxId) . ",
                " . $iShopId . ",
                " . $iLang . ",
                " . $oDb->quote($sSEOKeywords) . ",
                " . $oDb->quote($sSEODescription) . "
            )
            on duplicate key update
            oxkeywords = " . $oDb->quote($sSEOKeywords) . ", oxdescription = " . $oDb->quote($sSEODescription);

        return ToolsDB::execute($sSeoQuery);
    }

    /**
    * Returns static url for passed standard link (if available)
    *
    * @param string $sStdUrl standard Url
    * @param int    $iLang   active language (optional). default null
    * @param int    $iShopId active shop id (optional). default null
    *
    * @return string - objectId
    */
    protected static function _getIdForStaticUrl($sStdUrl, $iLang = null, $iShopId = null)
    {
        $aParams = [
            'oxstdurl' => $sStdUrl
        ];

        if (!is_null($iShopId)) {
            $aParams['oxshopid'] = $iShopId;
        }
        if (!is_null($iLang)) {
            $aParams['oxlang'] = $iLang;
        }

        $sObjectId = ToolsDB::getAnyId(
            'oxseo',
            $aParams,
            'oxobjectid'
        );

        return $sObjectId;
    }
}
