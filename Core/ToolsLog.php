<?php

/**
* @author    Mario Lorenz, www.the-real-world.de
* @copyright 2021 the-real-world.de
* @license   https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

namespace TheRealWorld\ToolsPlugin\Core;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\DatabaseProvider;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;
use TheRealWorld\ToolsPlugin\Core\ToolsConfig;

class ToolsLog
{
    /**
    * Log-Array
    * @param array
    */
    protected static $_aLog = [];

    /**
    * possible Log Levels
    * @param array
    */
    protected static $_aPossibleLogLevels = [
        'emergency', 'alert', 'critical', 'error', 'warning', 'info', 'notice', 'debug'
    ];

    /**
    * the default Log Level
    * @param string
    */
    protected static $_sDefaultLogLevel = 'info';

    /**
    * set Log entries
    *
    * @param string - $sMessage - Log Message -> StandardMessages would be translate
    * @param string - $sChannel - Log Channel
    * @param string - $sLogLevel - Log Level type
    *
    * @return boolean
    */
    public static function setLogEntry($sMessage = null, $sChannel = null, $sLogLevel = null)
    {
        $bResult = false;
        if ($sMessage) {
            $sMessage = Registry::getLang()->translateString($sMessage);

            $_aTmp = [
                'channel'  => $sChannel,
                'level'    => (in_array($sLogLevel, self::getPossibleLogLevels()) ? $sLogLevel : self::$_sDefaultLogLevel),
                'message'  => $sMessage,
                'datetime' => time()
            ];
            self::$_aLog[] = $_aTmp;

            $bResult = true;
        }
        return $bResult;
    }

    /**
    * set many Log entries
    *
    * @param string - $aOxMessages - Array with Log Messages
    * @param string - $sChannel - Log Channel
    * @param string - $sLogLevel - Log Level type
    *
    * @return boolean
    */
    public static function setLogEntries($aOxMessages = [], $sChannel = null, $sLogLevel = null)
    {
        $bResult = false;
        if (count($aOxMessages)) {
            foreach ($aOxMessages as $sMessage) {
                if (!$bResult = self::setLogEntry($sMessage, $sChannel, $sLogLevel)) {
                    break;
                }
            }
        }
        return $bResult;
    }

    /**
    * get Log entries
    *
    * @return array
    */
    public static function getLog()
    {
        return self::$_aLog;
    }

    /**
    * set Log entries
    *
    * @return boolean
    */
    public static function setLog($aLog = [])
    {
        $bResult = false;
        if (is_array($aLog)) {
            self::$_aLog = $aLog;
            $bResult = true;
        }
        return $bResult;
    }


    /**
    * get possible LogLevels
    *
    * @return array
    */
    public static function getPossibleLogLevels()
    {
        return self::$_aPossibleLogLevels;
    }

    /**
    * get Log entries
    *
    * @param string $sDelimiter - String delimiter for the Log-Entries
    *
    * @return string
    */
    public static function getLogAsText($sDelimiter = "\n")
    {
        $sResult = '';
        foreach (self::$_aLog as $aLogItem) {
            $sResult .= date('H:i:s', $aLogItem['datetime']) . ' ' . $aLogItem['message'] . $sDelimiter;
        }
        return $sResult;
    }

    /**
    * is Log entries
    *
    * @return boolean
    */
    public static function isLog()
    {
        $bResult = false;
        if (count(self::$_aLog)) {
            $bResult = true;
        }
        return $bResult;
    }

    /**
    * reset Log entries
    *
    * @return boolean
    */
    public static function resetLogEntry()
    {
        self::$_aLog = [];
        return true;
    }
}
