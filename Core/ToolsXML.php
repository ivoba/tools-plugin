<?php

/**
* @author    Mario Lorenz, www.the-real-world.de
* @copyright 2021 the-real-world.de
* @license   https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

namespace TheRealWorld\ToolsPlugin\Core;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;

class ToolsXML
{
    /**
    * the XML Writer/Reader obj
    * @param obj
    */
    protected static $_oXML = null;

    /**
    * the Export File
    * @param string
    */
    protected static $_sXMLFileName = null;

    /**
    * the Encoding of XML-File
    * @param string
    */
    protected static $_sXMLEncoding = 'UTF-8';

    /**
    * the Version of XML-File
    * @param string
    */
    protected static $_sXMLVersion = '1.0';

    /**
    * a optional File Path
    * @param string
    */
    protected static $_sXMLFilePath = null;

    /**
    * a possible XML String
    * @param string
    */
    protected static $_sXMLString = null;

    /**
    * a possible XML Object
    * @param object
    */
    protected static $_sXMLObj = null;

    /**
    * the Start Node
    * @param string
    */
    protected static $_sStartNode = null;

    protected static $_aStartNodeAttributes = null;

    /**
    * the pathtypes for possible xml-file-places
    * @param array
    */
    protected static $_aPathTypes = [
        'import', 'export'
    ];

    /**
    * the vars for find positions, offsets etc
    * @param int
    */
    protected static $_iCount = null;
    protected static $_iPos = null;

    /**
    * set the XML file name
    *
    * @param string  $sXMLFileName - the file name
    * @param boolean $bOverwrite   - overwrite the Name?
    * @param string  $sXMLFilePath - optional file path
    *
    * @return boolean
    */
    public static function setXMLFileName($sXMLFileName = '', $bOverwrite = false, $sXMLFilePath = null)
    {
        $bResult = false;
        if ((is_null(self::$_sXMLFileName) || $bOverwrite) && $sXMLFileName) {
            $bResult = true;
            self::$_sXMLFileName = $sXMLFileName;
        }
        if ((is_null(self::$_sXMLFilePath) || $bOverwrite) && !is_null($sXMLFilePath)) {
            $bResult = true;
            self::$_sXMLFilePath = $sXMLFilePath;
        }
        return $bResult;
    }

    /**
    * get the XML file name
    *
    * @param boolean $bWithPath - the name included with path?
    * @param string $sPathType - 'import', 'export' Decides between the paths
    *
    * @return string
    */
    public static function getXMLFileName($bWithPath = false, $sPathType = '')
    {
        $mResult = false;
        if (!is_null(self::$_sXMLFileName)) {
            $mResult = '';
            if ($bWithPath && in_array($sPathType, self::$_aPathTypes)) {
                $oConfig = Registry::getConfig();
                switch ($sPathType) {
                    case 'import':
                        // if we have a individual path, so we take this path
                        // else we take the compile path
                        $mResult .= (
                            !is_null(self::$_sXMLFilePath) ?
                            $oConfig->getConfigParam('sShopDir') . self::$_sXMLFilePath :
                            $oConfig->getConfigParam('sCompileDir')
                        );
                        break;
                    case 'export':
                        // if we have a individual path, so we take this path
                        // else we take the export path
                        $mResult .= $oConfig->getConfigParam('sShopDir');
                        $mResult .= (
                            !is_null(self::$_sXMLFilePath) ?
                            self::$_sXMLFilePath :
                            'export/'
                        );
                        break;
                }
            }
            $mResult .= self::$_sXMLFileName;
        }

        return $mResult;
    }

    /**
    * set the XML content - only use for small content
    *
    * @param string $sString - the xml content
    * @param boolean $bOverwrite - overwrite the StartNode?
    *
    * @return boolean
    */
    public static function setXMLString($sString = '', $bOverwrite = false)
    {
        $bResult = false;
        if ((is_null(self::$_sXMLString) || $bOverwrite) && $sString) {
            self::$_sXMLString = $sString;
            $bResult = true;
        }
        return $bResult;
    }

    /**
    * get the XML content
    *
    * @return string
    */
    public static function getXMLString()
    {
        $mResult = false;
        if (!is_null(self::$_sXMLString)) {
            $mResult = self::$_sXMLString;
        }
        return $mResult;
    }

    /**
    * set a XML object - only use for small content
    *
    * @param obj $oObj - the xml object
    *
    * @return boolean
    */
    public static function setXMLObj($oObj = '')
    {
        $bResult = false;
        if (is_object($oObj)) {
            $bResult = true;
            self::$_sXMLObj = $oObj;
        }
        return $bResult;
    }

    /**
    * get the XML content
    *
    * @return mixed (boolean / object)
    */
    public static function getXMLObj()
    {
        $mResult = false;
        if (!is_null(self::$_sXMLObj)) {
            $mResult = self::$_sXMLObj;
        }
        return $mResult;
    }

    /**
    * reset the XML content
    *
    * @return null
    */
    public static function resetXMLObj()
    {
        self::$_sXMLObj = null;
    }

    /**
    * set the XML start node
    *
    * @param string $sStartNode - the start node
    * @param boolean $bOverwrite - overwrite the StartNode?
    *
    * @return boolean
    */
    public static function setStartNode($sStartNode = '', $bOverwrite = false)
    {
        $bResult = false;
        if ((is_null(self::$_sStartNode) || $bOverwrite) && $sStartNode) {
            $bResult = true;
            self::$_sStartNode = $sStartNode;
        }
        return $bResult;
    }

    /**
    * set the XML start node Attributes
    *
    * @param array $aStartNodeAttributes - the start node attributes
    * @param boolean $bOverwrite - overwrite the StartNode?
    *
    * @return boolean
    */
    public static function setStartNodeAttributes($aStartNodeAttributes = [], $bOverwrite = false)
    {
        $bResult = false;
        if (
            (is_null(self::$_aStartNodeAttributes) || $bOverwrite) &&
            is_array($aStartNodeAttributes)
        ) {
            $bResult = true;
            self::$_aStartNodeAttributes = $aStartNodeAttributes;
        }
        return $bResult;
    }


    /**
    * set XML Version
    *
    * @param string $sXMLVersion
    *
    * @return null
    */
    public static function setXMLVersion($sXMLVersion = '')
    {
        if ($sXMLVersion) {
            self::$_sXMLVersion = $sXMLVersion;
        }
    }

    /**
    * set XML Version
    *
    * @param string $sXMLVersion
    *
    * @return null
    */
    public static function setXMLEncoding($sXMLEncoding = '')
    {
        if ($sXMLEncoding) {
            self::$_sXMLEncoding = $sXMLEncoding;
        }
    }

    /**
    * get the XML start node
    *
    * @return mixed (boolean / string)
    */
    public static function getStartNode()
    {
        $mResult = false;
        if (!is_null(self::$_sStartNode)) {
            $mResult = self::$_sStartNode;
        }
        return $mResult;
    }

    /**
    * get the XML start node attributes
    *
    * @return array
    */
    public static function getStartNodeAttributes()
    {
        $aResult = [];
        if (
            !is_null(self::$_aStartNodeAttributes) &&
            is_array(self::$_aStartNodeAttributes) &&
            count(self::$_aStartNodeAttributes)
        ) {
            $aResult = self::$_aStartNodeAttributes;
        }
        return $aResult;
    }

    /**
    * get XMLFile from Array - this is a fast version for limited data
    * if you�ve more data do the steps alone
    *
    * @param string $sXMLFileName         - the name of XML-File
    * @param string $sXMLFilePath         - optional file path
    * @param array  $aData                - the array with the data
    * @param string $sStartNode           - the start node
    * @param string $aStartNodeAttributes - optional start node attributes
    * @param string $sNode                - the normal nodes
    * @param string $bSendToBrowser       - send the file to the browser
    * @param string $sXMLEncoding         - optinal overwrite the Standard "UTF-8"
    *
    * @return string
    */
    public static function getXMLFileFromArray($sXMLFileName = '', $sXMLFilePath = null, $aData = [], $sStartNode = '', $aStartNodeAttributes = [], $sNode = '', $bSendToBrowser = true, $sXMLEncoding = null)
    {
        if (!is_null($sXMLEncoding)) {
            self::setXMLEncoding($sXMLEncoding);
        }
        $mResult = self::setXMLFileName($sXMLFileName, true, $sXMLFilePath);
        if ($mResult) {
            $mResult = self::setStartNode($sStartNode, true);
        }
        if ($mResult) {
            $mResult = self::setStartNodeAttributes($aStartNodeAttributes, true);
        }
        if ($mResult) {
            $mResult = self::setItemsToXML($aData, true, true, $sNode);
        }
        if ($mResult) {
            self::getXMLFile($bSendToBrowser);
        }
    }

    /**
    * get XMLString from Array - this is a fast version for limited data
    * if you�ve more data do the steps alone
    *
    * @param array  $aData                - the array with the data
    * @param string $sStartNode           - the start node
    * @param string $aStartNodeAttributes - optional start node attributes
    * @param string $sNode                - the normal nodes
    * @param string $sXMLEncoding         - optinal overwrite the Standard "UTF-8"
    *
    * @return string
    */
    public static function getXMLStringFromArray($aData = [], $sStartNode = '', $aStartNodeAttributes = [], $sXMLEncoding = null)
    {
        if (!is_null($sXMLEncoding)) {
            self::setXMLEncoding($sXMLEncoding);
        }
        $mResult = self::setStartNode($sStartNode, true);
        if ($mResult) {
            $mResult = self::setStartNodeAttributes($aStartNodeAttributes, true);
        }
        if ($mResult) {
            $mResult = self::setItemsToXML($aData, true, true);
        }
        if ($mResult) {
            $mResult = self::getXMLString();
        }
        return $mResult;
    }

    /**
    * get Array from XML - this is a fast version for limited data
    * if you�ve more data do the steps alone
    *
    * @param string $sXMLFileName - the name of XML-File
    * @param string $sXMLFilePath - path to XML-File
    * @param string $sStartNode   - the start node
    *
    * @return array
    */
    public static function getArrayFromXMLFile($sXMLFileName = '', $sXMLFilePath = null, $sStartNode = '')
    {
        self::resetXMLObj();

        $mResult = self::setXMLFileName($sXMLFileName, true, $sXMLFilePath);
        if ($mResult) {
            $mResult = self::setStartNode($sStartNode, true);
        }
        if ($mResult) {
            $mResult = self::collectItemsFromXML(null);
        }
        if ($mResult) {
            $mResult = self::convertXMLToArray(self::$_oXML);
        }
        return $mResult;
    }

    /**
    * get one Item from XML - the best way togehter with API-Answers
    *
    * @param string $sXMLString - the XMLString
    * @param string $sStartNode - the start node
    *
    * @return array
    */
    public static function getOneItemFromXMLString($sXMLString = '', $sStartNode = '')
    {
        self::resetXMLObj();

        $mResult = self::setXMLString($sXMLString, true);

        if ($mResult) {
            $mResult = self::setStartNode($sStartNode, true);

            if (!$mResult) {
                $mResult = self::getStartNode();
            }
        }
        if ($mResult) {
            $mResult = self::collectItemsFromXML(1);
        }
        if ($mResult) {
            $aXMLObj = self::getXMLObj();
            $mResult = $aXMLObj[0];
        }
        return $mResult;
    }

    /**
    * get the XML Content - maybe as Download
    *
    * @param string $bWithHeader
    *
    * @return mixed (download/string/boolean)
    */
    public static function getXMLFile($bWithHeader = true)
    {
        $mResult = false;
        if (($sFile = self::getXMLFileName(true, 'export')) && file_exists($sFile)) {
            if ($bWithHeader) {
                header('Content-type: text/xml');
                header('Content-Description: File Transfer');
                header('Content-Disposition: attachment; filename="' . self::getXMLFileName() . '"');
                header('Content-Type: application/octet-stream');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($sFile));
                readfile($sFile);
                Registry::getUtils()->showMessageAndExit("");
            } else {
                $mResult = file_get_contents($sFile);
            }
        }
        return $mResult;
    }

    /**
    * collect Items from a XML-File or a XML-String
    *
    * @param int    $iLimit  - the numbers of Elements the maximal return, null = no limit
    * @param int    $iOffset - the offset of iterated Elements
    *
    * @return boolean
    */
    public static function collectItemsFromXML($iLimit = 20, $iOffset = 0, $bLast = false)
    {
        $bResult = false;

        $sNodeName = self::getStartNode();
        $sString = self::getXMLString();
        $sFile = self::getXMLFileName(true, 'import');

        if ($sNodeName && ($sString || $sFile)) {
            self::$_iPos = 0;
            self::resetXMLObj();
            if ($sFile) {
                $sCheck = $sFile;
                $isFile = true;
            } else {
                $sCheck = $sString;
                $isFile = false;
            }
            $bCheck = self::_loadAndValidateXML($sCheck, $isFile);

            if ($bCheck) {
                self::$_iCount = 0;

                // move to the first node
                while (self::$_oXML->read() && self::$_oXML->name != $sNodeName);

                self::$_sXMLObj = [];

                while (self::$_oXML->name == $sNodeName) {

                    // found offset
                    if (self::$_iPos < $iOffset) {
                        self::$_iPos++;
                        self::$_oXML->next($sNodeName);
                        continue;
                    }

                    if ($oXmlResult = simplexml_load_string(self::$_oXML->readOuterXML(), 'SimpleXMLElement', LIBXML_NOCDATA)) {

                        $bResult = true;

                        self::$_sXMLObj[] = $oXmlResult;

                        self::$_iCount++;
                        self::$_iPos++;

                        // break by limit
                        if (!is_null($iLimit) && self::$_iCount > $iLimit) {
                            break;
                        }
                    }

                    // go to next Node
                    self::$_oXML->next($sNodeName);
                }
            }

            // only we have no result or it is the last, we close the XML
            if (!$bResult || $bLast) {
                self::$_oXML->close();
            }
        }
        return $bResult;
    }

    /**
    * set Items to a XML-File
    *
    * @param array  $aData    - the array with the data
    * @param string $bFirst   - has the data array the first element inside?
    * @param string $bLast    - has the data array the last element inside?
    * @param string $sXMLNode - optional XMLNode term
    *
    * @return boolean
    */
    public static function setItemsToXML($aData = [], $bFirst = true, $bLast = true, $sXMLNode = '')
    {
        $bResult = false;
        if ($sStartNode = self::getStartNode()) {
            $bNeedOpenXML = false;
            $bOpenXMLSuccess = false;

            if (is_null(self::$_oXML) || $bFirst) {
                self::$_oXML = new \XmlWriter();
                $bNeedOpenXML = true;
            }

            $sFile = self::getXMLFileName(true, 'export');

            if (
                $bFirst &&
                $sFile &&
                file_exists($sFile) &&
                is_file($sFile)
            ) {
                // if file exists and we start with creating, we delete the file
                unlink($sFile);
            }

            if ($bNeedOpenXML) {
                if ($sFile) {
                    $bOpenXMLSuccess = self::$_oXML->openURI($sFile);
                } else {
                    $bOpenXMLSuccess = self::$_oXML->openMemory();
                }

                self::$_oXML->setIndent(true);

                self::$_oXML->startDocument(self::$_sXMLVersion, self::$_sXMLEncoding);
                self::$_oXML->startElement($sStartNode);
                if ($aStartNodeAttributes = self::getStartNodeAttributes()) {
                    foreach ($aStartNodeAttributes as $sAttribute => $sValue) {
                        self::$_oXML->startAttribute($sAttribute);
                        self::$_oXML->text($sValue);
                        self::$_oXML->endAttribute();
                    }
                }
            } else {
                $bOpenXMLSuccess = true;
            }

            if (is_array($aData) && count($aData) && $bOpenXMLSuccess) {
                $bResult = true;
                // if we have a numeric counted start array, so we sanitized them
                if (isset($aData[0])) {
                    $sXMLNode = ($sXMLNode ? $sXMLNode : $sStartNode . 'item');
                    $aData = [$sXMLNode => $aData];
                }
                self::_writeXMLElement($aData);
                if ($sFile) {
                    self::$_oXML->flush();
                }
            }

            // in the last step we close the xml structure
            if ($bLast) {
                $bResult = true;

                self::$_oXML->endElement();
                self::$_oXML->endDocument();
                // if we have no file we get the memory-buffer
                if (!$sFile) {
                    self::$_sXMLString = self::$_oXML->outputMemory();
                }
            }
        }
        return $bResult;
    }

    /**
    * convert a XML-Object to an array
    *
    * @param object $oXml - SimpleXMLElement XML-Object
    *
    * @return Array
    */
    public static function convertXMLToArray(\SimpleXMLElement $oXml)
    {
        $aResult = [];
        $oNodes = $oXml->children();
        $oAttributes = $oXml->attributes();

        if (0 !== count($oAttributes)) {
            foreach ($oAttributes as $sAttrName => $sAttrValue) {
                $aResult['@attributes'][$sAttrName] = strval($sAttrValue);
            }
        }

        if (0 === $oNodes->count()) {
            if ($oXml->attributes()) {
                $aResult['value'] = strval($oXml);
            } else {
                $aResult = strval($oXml);
            }
            return $aResult;
        }

        foreach ($oNodes as $oNodeName => $oNodeValue) {
            if (count($oNodeValue->xpath('../' . $oNodeName)) < 2) {
                $aResult[$oNodeName] = self::convertXMLToArray($oNodeValue);
                continue;
            }

            $aResult[$oNodeName][] = self::convertXMLToArray($oNodeValue);
        }
        return $aResult;
    }

    /**
    * Write keys in $aData prefixed with @ as XML attributes, if $aData is an array.
    * When an @ prefixed key is found, a '' key is expected to indicate the element itself.
    *
    * @param array  $aData - with attributes filtered out
    *
    * @return mixed $aData with attributes filtered out
    */
    protected static function _writeXMLElementAsAttribute($aData = [])
    {
        if (is_array($aData)) {
            $nonAttributes = [];
            foreach ($aData as $mKey => $val) {
                // handle an attribute with elements
                if ($mKey[0] == '@') {
                    self::$_oXML->writeAttribute(substr($mKey, 1), $val);
                } elseif ($mKey == '') {
                    if (is_array($val)) {
                        $nonAttributes = $val;
                    } else {
                        self::$_oXML->text((string)$val);
                    }
                }

                // ignore normal elements
                else {
                    $nonAttributes[$mKey] = $val;
                }
            }
            return $nonAttributes;
        } else {
            return $aData;
        }
    }

    /**
    * Write XML as per Associative Array
    *
    * @param array  $aData - Associative Data Array
    *
    * @return boolean
    */
    protected static function _writeXMLElement($aData = [])
    {
        foreach ($aData as $mKey => $mValue) {
            if (is_array($mValue) && isset($mValue[0])) {
                // numeric array
                foreach ($mValue as $mItemValue) {
                    if (is_array($mItemValue)) {
                        self::$_oXML->startElement($mKey);
                        $mItemValue = self::_writeXMLElementAsAttribute($mItemValue);
                        self::_writeXMLElement($mItemValue);
                        self::$_oXML->endElement();
                    } else {
                        $mItemValue = self::_writeXMLElementAsAttribute($mItemValue);
                        self::_wrapCData($mKey, (string)$mItemValue);
                    }
                }
            } elseif (is_array($mValue)) {
                // associative array
                self::$_oXML->startElement($mKey);
                $mValue = self::_writeXMLElementAsAttribute($mValue);
                self::_writeXMLElement($mValue);
                self::$_oXML->endElement();
            } else {
                //scalar
                $mValue = self::_writeXMLElementAsAttribute($mValue);
                self::_wrapCData($mKey, (string)$mValue);
            }
        }
        return true;
    }

    /**
    * Check if CData Wrapper ist needed
    *
    * @param string $sNode - XML Node Name
    * @param string $sString - string to wrap
    *
    * @return null
    */
    protected static function _wrapCData($sNode, $sString)
    {
        $bCDataNeeded = false;
        $oStr = getStr();
        if ($oStr->strpos($sString, '<') !== false || $oStr->strpos($sString, '&') !== false) {
            $bCDataNeeded = true;
        }
        if ($bCDataNeeded) {
            self::$_oXML->startElement($sNode);
            self::$_oXML->writeCData($sString);
            self::$_oXML->endElement();
        } else {
            self::$_oXML->writeElement($sNode, (string)$sString);
        }
    }

    /**
    * load and validate XML (not DTD valid)
    *
    * @param string  $sXML    - string or file to check
    * @param boolean $bIsFile - is the string a filename?
    *
    * @return boolean
    */
    protected static function _loadAndValidateXML($sXML, $bIsFile = true)
    {
        if (is_null(self::$_oXML)) {
            self::$_oXML = new \XMLReader();
        }

        if ($bIsFile) {
            // we check if files exist
            if ($bResult = file_exists($sXML)) {
                self::$_oXML->open($sXML, self::$_sXMLEncoding, LIBXML_NOCDATA);
            }
        } else {
            $bResult = true;
            self::$_oXML->xml($sXML, self::$_sXMLEncoding, LIBXML_NOCDATA);
        }

        if ($bResult) {
            self::$_oXML->setParserProperty(\XMLReader::VALIDATE, true);
            $bResult = self::$_oXML->isValid();
            self::$_oXML->setParserProperty(\XMLReader::VALIDATE, false);
        }
        return $bResult;
    }
}
