<?php

/**
* @author    Mario Lorenz, www.the-real-world.de
* @copyright 2021 the-real-world.de
* @license   https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

namespace TheRealWorld\ToolsPlugin\Core;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\TableViewNameGenerator;
use TheRealWorld\ToolsPlugin\Core\ToolsConfig;

class ToolsDB
{
    /**
    * get OXID id or others from given table
    *
    * @param string  - $sTable - The table where i found the sObject
    * @param array   - $aWhere - an array for and associated conditions
    * @param mixed   - $mObject - search for a special column (string) or many columns (array)
    * @param integer - $iLangId - search in a special language?
    * @param boolean - $bColognePhonetics - search with Cologne Phonetics
    *
    * @return mixed - normally string
    */
    public static function getAnyId($sTable, $aWhere = [], $mObject = 'oxid', $iLangId = null, $bColognePhonetics = false)
    {
        $mResult = null;
        if ($sTable && is_array($aWhere)) {
            $oDb = DatabaseProvider::getDb();

            $viewNameGenerator = Registry::get(TableViewNameGenerator::class);

            $mResult = false;

            if (is_array($mObject)) {
                $sSearch = implode(', ', $mObject);
            } else {
                $sSearch = $mObject;
            }

            $sSelect = "select " . $sSearch . "
                from " . $viewNameGenerator->getViewName($sTable, $iLangId) . "
                where 1 ";

            if (is_array($aWhere)) {
                foreach ($aWhere as $sName => $sValue) {
                    if ($bColognePhonetics) {
                        $sSelect .= " and ( fnc_cologne_match(" . $oDb->quote($sValue) . ", " . $sName . ", ' ') = 1 ) ";
                    } else {
                        $sSelect .= " and " . $sName . " = " . $oDb->quote($sValue);
                    }
                }
            }
            $sSelect .= " limit 1";

            if (is_array($mObject)) {
                $oList = oxNew(\OxidEsales\Eshop\Core\Model\ListModel::class);
                //$oList->init($sTable);
                $oList->selectString($sSelect);
                if ($oList->count()) {
                    $mResult = [];
                    $oRow = $oList->current();
                    foreach ($mObject as $sKey) {
                        $mResult[$sKey] = $oRow->{'__' . $sKey}->value;
                    }
                }
            } else {
                $mResult = $oDb->getOne($sSelect);
            }
        }
        return $mResult;
    }

    /**
    * convertDB2OxParams
    *
    * convert a Oxid DB-Array to a typical Oxid Params-Array
    *
    * @param array $aData
    * @param string $sTable
    *
    * @return array
    */
    public static function convertDB2OxParams($aData = [], $sTable = null)
    {
        $aDataResult = [];
        if (is_array($aData) && $sTable) {
            foreach ($aData as $sKey => $sValue) {
                $sNewKey = Str::getStr()->strtolower($sTable . '__' . $sKey);
                $aDataResult[$sNewKey] = $sValue;
            }
        }
        return $aDataResult;
    }

    /**
    * Check if table or table column exists
    *
    * @param  $sTable - Name of table
    * @param  $sColumn - Name of Column
    *
    * @return boolean
    */
    public static function tableColumnExists($sTable = '', $sColumn = '')
    {
        $bResult = false;
        if ($sTable) {
            $oDb = DatabaseProvider::getDb();

            if ($sColumn) {
                $sSql = "show columns from $sTable like " . $oDb->quote($sColumn);
            } else {
                $sSql = "show tables like " . $oDb->quote($sTable);
            }

            $oResults = $oDb->select($sSql);
            if ($oResults != false && $oResults->count() > 0) {
                $bResult = true;
            }
        }
        return $bResult;
    }

    /**
    * Check if index exists
    *
    * @param  $sTable - Name of table
    * @param  $sIndex - Name of index
    *
    * @return boolean
    */
    public static function tableIndexExists($sTable = '', $sIndex = '')
    {
        $bResult = false;
        if ($sTable && $sIndex) {
            $oDb = DatabaseProvider::getDb();

            $sSql = "show index from $sTable where key_name like " . $oDb->quote($sIndex);

            $oResults = $oDb->select($sSql);
            if ($oResults != false && $oResults->count() > 0) {
                $bResult = true;
            }
        }
        return $bResult;
    }

    /**
    * set a table as Multilangual and Multishop
    *
    * @param  $sTable - Name of table
    *
    * @return boolean
    */
    public static function setTableMulti($sTable = '')
    {
        if ($sTable) {
            self::setTableMultiLang($sTable);
            self::setTableMultiShop($sTable);
        }
        return true;
    }

    /**
    * delete a table as Multilangual and Multishop
    *
    * @param  $sTable - Name of table
    *
    * @return boolean
    */
    public static function deleteTableMulti($sTable = '')
    {
        if ($sTable) {
            self::deleteTableMultiLang($sTable);
            self::deleteTableMultiShop($sTable);
        }
        return true;
    }

    /**
    * set a table as Multilangual
    *
    * @param  $sTable - Name of table
    *
    * @return boolean
    */
    public static function setTableMultiLang($sTable = '')
    {
        if ($sTable) {
            $oConfig = Registry::getConfig();

            foreach($oConfig->getShopIds() as $iShopId) {
                $aMultiLangTables = $oConfig->getShopConfVar('aMultiLangTables', $iShopId);
                if (!is_array($aMultiLangTables)) {
                    $aMultiLangTables = Registry::getLang()->getMultiLangTables();
                }
                if (!in_array($sTable, $aMultiLangTables)) {
                    $aMultiLangTables[] = $sTable;
                }
                ToolsConfig::saveConfigParam("aMultiLangTables", $aMultiLangTables, 'arr', '', $iShopId);
            }
        }
        return true;
    }

    /**
    * delete a table as Multilangual
    *
    * @param  $sTable - Name of table
    *
    * @return boolean
    */
    public static function deleteTableMultiLang($sTable = '')
    {
        if ($sTable) {
            $oConfig = Registry::getConfig();

            foreach($oConfig->getShopIds() as $iShopId) {
                $aMultiLangTables = $oConfig->getShopConfVar('aMultiLangTables', $iShopId);
                if (!is_array($aMultiLangTables)) {
                    $aMultiLangTables = Registry::getLang()->getMultiLangTables();
                }
                if (in_array($sTable, $aMultiLangTables)) {
                    $aMultiLangTables = array_diff($aMultiLangTables, [$sTable]);
                    ToolsConfig::saveConfigParam("aMultiLangTables", $aMultiLangTables, 'arr', '', $iShopId);
                }
            }
        }
        return true;
    }

    /**
    * set a table as Multishop
    *
    * @param  $sTable - Name of table
    *
    * @return boolean
    */
    public static function setTableMultiShop($sTable = '')
    {
        if ($sTable && ToolsConfig::isMultiShop()) {
            $oConfig = Registry::getConfig();

            foreach($oConfig->getShopIds() as $iShopId) {

                // manipulate Option "aMultiShopTables"
                $aMultiShopTables = $oConfig->getShopConfVar('aMultiShopTables', $iShopId);

                if (!is_array($aMultiShopTables)) {
                    $aMultiShopTables = $oConfig->getConfigParam('aMultiShopTables');
                }

                if (!in_array($sTable, $aMultiShopTables)) {
                    $aMultiShopTables[] = $sTable;
                }
                ToolsConfig::saveConfigParam(
                    'aMultiShopTables',
                    $aMultiShopTables,
                    'arr',
                    '',
                    $iShopId
                );

                // create blMallInherit_??? Variable
                $sMallInheritVar = 'blMallInherit_' . $sTable;
                $bMallInherit = $oConfig->getShopConfVar($sMallInheritVar, $iShopId);

                ToolsConfig::saveConfigParam(
                    $sMallInheritVar,
                    $bMallInherit,
                    'bool',
                    '',
                    $iShopId
                );
            }
        }
        return true;
    }

    /**
    * delete a table as Multishop
    *
    * @param  $sTable - Name of table
    *
    * @return boolean
    */
    public static function deleteTableMultiShop($sTable = '')
    {
        if ($sTable && ToolsConfig::isMultiShop()) {
            $oConfig = Registry::getConfig();

            foreach($oConfig->getShopIds() as $iShopId) {

                // manipulate Option "aMultiShopTables"
                $aMultiShopTables = $oConfig->getShopConfVar('aMultiShopTables', $iShopId);

                if (!is_array($aMultiShopTables)) {
                    $aMultiShopTables = $oConfig->getConfigParam('aMultiShopTables');
                }

                if (in_array($sTable, $aMultiShopTables)) {
                    $aMultiLangTables = array_diff($aMultiLangTables, [$sTable]);
                    ToolsConfig::saveConfigParam(
                        'aMultiShopTables',
                        $aMultiShopTables,
                        'arr',
                        '',
                        $iShopId
                    );
                };

                // delete blMallInherit_??? Variable
                $sMallInheritVar = 'blMallInherit_' . $sTable;

                $sSql = "delete from `oxconfig`
                    and `oxmodule` = ''
                    and `oxvarname` = " . $oDb->quote($sMallInheritVar);
                ToolsDB::execute($sSql);
            }
        }
        return true;
    }

    /**
    * get a Array with Columns
    *
    * @param string $sTableName = Name of the Table
    * @param string $sColumnName = Name of the TableColumn
    *
    * @return array
    */
    public static function showTableColumn($sTableName = '', $sColumnName = '')
    {
        $aResult = [];

        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $sSelect = "show columns
            from {$sTableName} " .
            ($sColumnName ? " like " . $oDb->quote($sColumnName)  : '');

        $oResults = $oDb->select($sSelect);
        if ($oResults != false && $oResults->count() > 0) {
            while (!$oResults->EOF) {
                $aRow = $oResults->getFields();
                $aResult[] = $aRow;
                $oResults->fetchRow();
            }
        }
        return $aResult;
    }

    /**
    * get a Array with Enum Values of a Field
    *
    * @param string $sTableName = Name of the Table
    * @param string $sColumnName = Name of the TableColumn
    *
    * @return array
    */
    public static function showEnumValues($sTableName = '', $sColumnName = '')
    {
        $aResult = [];
        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $sSelect = "show columns from `$sTableName` like " . $oDb->quote($sColumnName);

        $oResults = $oDb->select($sSelect);
        if ($oResults != false && $oResults->count() > 0) {
            $aRow = $oResults->getFields();
            if (preg_match('/enum\((.*)\)$/', $aRow['Type'], $aMatches)) {
                $aResult = str_getcsv($aMatches[1], ',', "'");
            }
        }
        return $aResult;
    }

    /**
    * delete Columns from table
    *
    * @param string $sTable = Name of the Table
    * @param string $sColumns = Array with TableColumns for delete
    * @param boolean $bCleanDbCache = Should clean the DB Cache?
    *
    * @return boolean
    */
    public static function deleteTableColumn($sTable = '', $sColumns = [], $bCleanDbCache = false)
    {
        $result = false;
        if (count($sColumns)) {
            foreach ($sColumns as $sColumn => $sColumnDescription) {
                $sDelete = "alter table {$sTable} drop {$sColumn}";
                self::execute($sDelete);
            }
            if ($bCleanDbCache) {
                self::_cleanDbCache($sTable);
            }
            $result = true;
        }

        return $result;
    }

    /**
    * insert Columns in Table table
    *
    * @param string $sTable = Name of the Table
    * @param string $aColumns = Array with TableColumns for insert array
    *             [0 => ['columnname' => ..., 'definition' => ..., 'comment' => ..., 'suffix' => ...]]
    * @param boolean $bCleanDbCache = Should clean the DB Cache?
    *
    * @return boolean
    */
    public static function insertTableColumn($sTable = '', $aColumns = [], $bCleanDbCache = false)
    {
        $result = false;
        if (count($aColumns)) {
            foreach ($aColumns as $iKey => $aColumnDescription) {
                $sSql = "alter table {$sTable} add `" . $aColumnDescription['columnname'] . "` " . $aColumnDescription['definition'] . " comment '" . $aColumnDescription['comment'] . "' " . $aColumnDescription['suffix'];
                self::execute($sSql);
            }
            if ($bCleanDbCache) {
                self::_cleanDbCache($sTable);
            }
            $result = true;
        }
        return $result;
    }

    /**
    * get the field Size of a table column
    *
    * @param string $sTable = Name of the Table
    * @param string $sField = Name of the Table-Field
    *
    * @return string
    */
    public static function getFieldSize($sTable, $sField)
    {
        $sDbName = Registry::getConfig()->getShopConfVar('dbName');
        $oDb = DatabaseProvider::getDb();
        $sSql = "select character_maximum_length
            from information_schema.columns
            where table_schema = " . $oDb->quote($sDbName) . "
            and table_name = " . $oDb->quote($sTable) . "
            and column_name = " . $oDb->quote($sField);
        return $oDb->getOne($sSql);
    }

    /**
    * get the name of Tables with a special prefix
    *
    * @param string $sTable = Name of the Table
    *
    * @return mixed boolean / array
    */
    public static function getTableWithPrefix($sTable)
    {
        $mResult = false;

        $oDb = DatabaseProvider::getDb();
        $sSql = "show tables like '{$sTable}%'";

        if ($aTables = $oDb->getAll($sSql)) {
            $mResult = [];
            foreach ($aTables as $aTable) {
                $mResult[] = $aTable[0];
            }
        }
        return $mResult;
    }

    /**
    * set the field Size of a table column
    *
    * @param string $sTable = Name of the Table
    * @param string $sField = Name of the Table-Field
    * @param int    $iSize  = new Size of the column
    *
    * @return boolean
    */
    public static function setFieldSize($sTable, $sField, $iSize)
    {
        $sSql = "alter table " . $sTable . "
            modify " . $sField . "
            varchar(" . $iSize . ")";
        return self::execute($sSql);
    }

    /**
    * get the field Informations of a table column
    *
    * @param string $sTable = Name of the Table
    * @param string $sField = Name of the Table-Field
    * @param string $sInformation = e.g. column_type, comments etc.
    *
    * @return string
    */
    public static function getFieldInformation($sTable, $sField, $sInformation = 'column_type')
    {
        $sDbName = Registry::getConfig()->getShopConfVar('dbName');
        $oDb = DatabaseProvider::getDb();
        $sSql = "select " . $sInformation . "
            from information_schema.columns
            where table_schema = " . $oDb->quote($sDbName) . "
            and table_name = " . $oDb->quote($sTable) . "
            and column_name = " . $oDb->quote($sField);
        return $oDb->getOne($sSql);
    }

    /**
    * modified the field of a table column
    *
    * @param string $sTable   = Name of the Table
    * @param string $sField   = Name of the Table-Field
    * @param string $sType    = New Type
    * @param string $sDefault = New Default
    * @param string $sComment = New Comment, if empty, trying to keep the old comment (optional)
    * @param string $bNotNull = Null? (optional)
    *
    * @return string
    */
    public static function changeField($sTable, $sField, $sType, $sDefault, $sComment = '', $bNotNull = true)
    {
        if (empty($sComment)) {
            $sComment = self::getFieldInformation($sTable, $sField, 'column_comment');
        }

        $sSql = "alter table " . $sTable . "
            modify " . $sField . " " . $sType . " " .
            ($bNotNull ? "not null" : "") .
            ($sDefault ? " default '" . $sDefault . "' " : "") .
            ($sComment ? " comment '" . $sComment . "' " : "");
        return self::execute($sSql);
    }

    /**
    * execute a SQL. On Error catch an Exception
    *
    * @return boolean/integer (integer Number of rows affected by the SQL statement)
    *
    */
    public static function execute($sSql)
    {
        $mResult = false;
        try {
            $mResult = DatabaseProvider::getDb()->execute($sSql);
        } catch (\Exception $e) {
            Registry::getUtilsView()->addErrorToDisplay($e->getMessage());
        }
        return $mResult;
    }

    /**
    * create an Single-Table SQL for Updates or Inserts
    * Use it only for simple update functions, without dependencies.
    * When it comes to a speed advantage.
    * Otherwise, always perform the updates object oriented.
    *
    * @param string $sTable        = Name of the Table
    * @param array $aData          = array with Data for Updates
    * @param string $sAction       = SQL-Action (insert|update)
    *                                [field1 => value1, field2 => value2]
    * @param array $aConditions    = array with Data for the Conditions
    *                                [
    *                                  ['operator' => 'and|or', 'condition' => '=|>|<|>=|<=', 'field' => field1, 'value' => 'value1', 'noquote' => true],
    *                                  ['operator' => 'and|or', 'condition' => '=|>|<|>=|<=', 'field' => field2, 'value' => 'value2', 'noquote' => false]
    *                                ]
    * @param boolean $bCheckShopId = check also the shopid in the SQL?
    *
    * @return boolean
    */
    public static function createSingleTableExecuteSql($sTable = '', $aData = [], $sAction = 'insert', $aConditions = null, $bCheckShopId = true)
    {
        $sResult = '';

        if (
            $sTable
            && count($aData)
            && in_array($sAction, ['insert', 'update'])
        ) {
            $sSql = ($sAction == 'update' ? $sAction : 'insert into');

            $sSql .= " {$sTable} set ";

            array_walk($aData, function (&$mValue, &$mKey) {
                if (Str::getStr()->strtolower($mValue) !== 'now()' && !is_numeric($mValue)) {
                    $mValue = DatabaseProvider::getDb()->quote($mValue);
                }
                $mValue = " {$mKey} = " . $mValue;
            });

            $sSql .= implode(', ', $aData);

            $sCondition = "";

            if (is_array($aConditions) && count($aConditions)) {
                $bFirst = true;
                foreach ($aConditions as $iKey => $aCondition) {
                    // check if all necessary keys exists
                    if (
                        count(
                            array_diff_key(
                                array_flip(
                                    ['operator', 'condition', 'field', 'value', 'noquote']
                                ),
                                $aCondition
                            )
                        ) == 0
                    ) {
                        if ($bFirst) {
                            $bFirst = false;
                        } else {
                            $sCondition .= " " . $aCondition['operator'];
                        }
                        $sCondition .= " {$aCondition['field']} " . $aCondition['condition'] . " "
                            . ($aCondition['noquote'] ? $aCondition['value'] : DatabaseProvider::getDb()->quote($aCondition['value']));
                    }
                }
            }

            if ($sCondition) {
                if ($bCheckShopId) {
                    $sCondition .= " and `oxshopid` = " . Registry::getConfig()->getShopId();
                }

                $sSql .= " where " . $sCondition;
            }
            $sResult = $sSql;
        }
        return $sResult;
    }

    /**
    * clean the DB Cache
    *
    * @param string $sTable = Name of the Table
    *
    */
    private static function _cleanDbCache($sTable = '')
    {
        if ($sTable) {
            $oMetaData = oxNew(\OxidEsales\Eshop\Core\DbMetaDataHandler::class);
            $oMetaData->updateViews([$sTable]);

            $aFiles = glob(Registry::getUtils()->getCacheFilePath(null, true) . 'oxpec_' . $sTable . '_allfields_*.txt');
            if (is_array($aFiles)) {
                foreach ($aFiles as $sFile) {
                    @unlink($sFile);
                }
            }
        }
    }
}
