<?php

/**
* @author    Mario Lorenz, www.the-real-world.de
* @copyright 2021 the-real-world.de
* @license   https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

namespace TheRealWorld\ToolsPlugin\Core;

use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;
use OxidEsales\Eshop\Core\ShopVersion;
use OxidEsales\Eshop\Core\Module\ModuleList;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;
use OxidEsales\EshopCommunity\Internal\Container\ContainerFactory;
use OxidEsales\EshopCommunity\Internal\Framework\Module\Setting\Setting;
use OxidEsales\EshopCommunity\Internal\Framework\Module\Configuration\Bridge\ModuleConfigurationDaoBridgeInterface;

class ToolsConfig
{
    /**
    * Possible Moduls Option Types
    * @param array
    */
    protected static $_aPossibleOptionTypes = ['select', 'bool', 'num', 'str', 'arr', 'aarr'];

    /**
    * exclude internal Shop Configs
    * @param array
    */
    protected static $_aExcludeInternalShopConfigs = [
        'activeModules', 'aModuleaModuleControllers', 'aModuleaModuleEvents', 'aModuleaModuleExtensions',
        'aModuleaModuleFiles', 'aModuleaModulePaths', 'aModuleaModuleTemplates', 'aModuleaModuleVersions',
        'aModuleaModuleVersions', 'aModuleEvents', 'aModuleExtensions', 'aModuleFiles', 'aModulePaths',
        'aModules', 'aModuleTemplates', 'aModuleVersions', 'aMultiLangTables', 'aServersData',
        'moduleSmartyPluginDirectories', 'aModuleControllers', 'aDisabledModules'
    ];

    /**
    * exclude Shop Values from shoptable
    * @param array
    */
    protected static $_aExcludeShopTableValues = [
        'oxid', 'oxadbutlerid', 'oxaffilinetid', 'oxsuperclicksid', 'oxedition', 'oxversion',
        'oxaffiliweltid', 'oxaffili24id', 'oxtimestamp'
    ];

    /**
    * Upload a File
    *
    * @param string  $sRequestFileName   - Name of the request-Filename
    * @param string  $sRequestFileTarget - Name of the request-File Target
    * @param boolean $bResultWithPath    - the Result is a file name. Result with path?
    *
    * @return string - the path + name of uploaded file
    */
    public static function uploadFile($sRequestFileName = '', $sRequestFileTarget = '', $bResultWithPath = false)
    {
        $mResult = false;
        $oConfig = Registry::getConfig();

        if ($sRequestFileName) {
            $aFile = $oConfig->getUploadedFile($sRequestFileName);
            if (isset($aFile['name']) && $aFile['name']) {
                $sFileUploadTarget = '';
                if ($sRequestFileTarget) {
                    $sFileUploadTarget .= $oConfig->getConfigParam('sShopDir') . $sRequestFileTarget;
                }
                if (!$sFileUploadTarget || !is_dir($sFileUploadTarget)) {
                    $sFileUploadTarget = $oConfig->getConfigParam('sCompileDir');
                }
                $sFileName = basename($aFile['name']);
                move_uploaded_file($aFile['tmp_name'], $sFileUploadTarget . $sFileName);
                $mResult = ($bResultWithPath ? $sFileUploadTarget : '') . $sFileName;
            }
        }
        return $mResult;
    }

    /**
    * Save Admin Options
    *
    * @param string $sRequestArrayName   - Name of the request-Parameter
    * @param string $aRequestFiles       - Array for handling of fileuploads
    *                                      (e.g.: [0 => [
    *                                        'name' => 'varname of <input type="file" name="...">',
    *                                        'target' => 'shop root relative target dir',
    *                                        'varname' => 'varname of config, where the filename are pushed'
    *                                      ]];
    *
    * @return boolean
    */
    public static function saveAdminOptions($sRequestArrayName = '', $aRequestFiles = [])
    {
        $bResult = false;
        $oConfig = Registry::getConfig();

        $aOptions = $oConfig->getRequestParameter($sRequestArrayName);

        if (is_array($aRequestFiles)) {
            foreach ($aRequestFiles as $aRequestFile) {
                if (array_key_exists('name', $aRequestFile) && array_key_exists('target', $aRequestFile)) {
                    $sUploadResult = self::uploadFile($aRequestFile['name'], $aRequestFile['target']);
                    if (array_key_exists('varname', $aRequestFile) && $aRequestFile['varname'] && is_array($aOptions) && $sUploadResult) {
                        $aOptions[$aRequestFile['varname']] = $sUploadResult;
                    }
                }
            }
        }

        if (is_array($aOptions)) {
            // The name of the variable controls the correct storage of the options
            // Example: bool.ModuleName.VarName
            // 1) type (bool, num, str, arr, aarr)
            // 2) the Name of the Module
            // 3) the Name of the Variable
            foreach ($aOptions as $sKey => $mVarVal) {
                list($sVarType, $sModule, $sVarName) = array_pad(explode('.', $sKey), 3, '');
                if (in_array($sVarType, self::$_aPossibleOptionTypes)) {
                    // prepare array options
                    if (in_array($sVarType, ['arr', 'aarr'])) {
                        if (!is_array($mVarVal)) {
                            $mVarVal = explode("\n", $mVarVal);

                            if ($sVarType == 'aarr') {
                                $mVarValNew = [];
                                foreach ($mVarVal as $sLine) {
                                    $sLine = trim($sLine);
                                    if ($sLine != "" && Str::getStr()->preg_match("/(.+)=>(.+)/", $sLine, $aRegs)) {
                                        $sKey = trim($aRegs[1]);
                                        $sVal = trim($aRegs[2]);
                                        if ($sKey != "" && $sVal != "") {
                                            $mVarValNew[$sKey] = $sVal;
                                        }
                                    }
                                }
                                $mVarVal = $mVarValNew;
                            }
                        }
                        $mVarVal = array_filter($mVarVal);
                    }

                    self::saveConfigParam($sVarType . $sModule . $sVarName, $mVarVal, $sVarType, $sModule);
                }
            }
            $bResult = true;
        }
        return $bResult;
    }

    /**
    * Save a var
    *
    * @param string $sName - name of Variable
    * @param mixed $mVarVal - Values
    * @param string $sVarType - Valuetype
    * @param string $sModule - Modulename
    * @param int $iShopId - ShopId
    *
    * @return boolean
    */
    public static function saveConfigParam($sName, $mVarVal, $sVarType = 'str', $sModule = '', $iShopId = null)
    {
        $oConfig = Registry::getConfig();
        $oConfig->setConfigParam($sName, $mVarVal);

        if (!$sVarType) {
            $sVarType = ToolsDB::getAnyId('oxconfig', ['oxvarname' => $sName], 'oxvartype');
        }

        if (!$sModule) {
            $sModule = ToolsDB::getAnyId('oxconfig', ['oxvarname' => $sName], 'oxmodule');
        }

        // add prefix if not exists
        $sModuleWithPrefix = $sModule;
        if ($sModule && !strpos($sModule, ':')) {
            $sModuleWithPrefix = 'module:' . $sModule;
        }
        $sModuleWithPrefix = Str::getStr()->strtolower($sModuleWithPrefix);

        // remove prefix
        $sModule = preg_replace('/^[^:]*:\s*/', '', $sModule);
        $sModule = Str::getStr()->strtolower($sModule);

        // save Module Options additional in OXID >= v6.2 as Bridge-Configuration
        if (
            !self::isDBConfig() &&
            strpos($sModuleWithPrefix, 'module:') !== false
        ) {
            $oModuleConfigurationDaoBridge = ContainerFactory::getInstance()->getContainer()->get(ModuleConfigurationDaoBridgeInterface::class);
            $oModuleConfiguration = $oModuleConfigurationDaoBridge->get($sModule);
            $oModuleSetting = $oModuleConfiguration->getModuleSetting($sName);
            $oModuleSetting->setValue($mVarVal);
            $oModuleConfigurationDaoBridge->save($oModuleConfiguration);
        }

        $bResult = $oConfig->saveShopConfVar(
            $sVarType,
            $sName,
            $mVarVal,
            $iShopId,
            $sModuleWithPrefix
        );

        return $bResult;
    }

    /**
    * Return if a Module exsists in a needed Version
    *
    * @param string  $sModule  - Modulename
    * @param string  $sVersion - Version
    * @param boolean $bActive  - is it necessary that the module is active?
    *
    * @return string
    */
    public static function getModuleExists($sModule = '', $sVersion = '', $bActive = true)
    {
        $bResult = false;
        if ($sModule) {
            $oModuleList = oxNew(ModuleList::class);
            $aModules = $oModuleList->getModuleConfigParametersByKey(ModuleList::MODULE_KEY_VERSIONS);
            if (array_key_exists($sModule, $aModules)) {
                $bResult = false;

                if (($sVersion && version_compare($aModules[$sModule], $sVersion, '>=')) || !$sVersion) {
                    $bResult = true;
                }
                if ($bResult && $bActive) {
                    $aDisabledModules = Registry::getConfig()->getConfigParam("aDisabledModules");
                    if (is_array($aDisabledModules) && in_array($sModule, $aDisabledModules)) {
                        $bResult = false;
                    }
                }
            }
        }
        return $bResult;
    }

    /**
    * get a Options Array and validate it
    *
    * @param string sOptionsArrayName - The Name of the option
    *
    * @return array
    */
    public static function getOptionsArray($sOptionsArrayName = '')
    {
        $aOptions = Registry::getConfig()->getConfigParam($sOptionsArrayName);
        return (is_array($aOptions) ? $aOptions : []);
    }

    /**
    * Returns the Default Nr. of Items (e.g. articles) per page
    *
    * @return integer
    */
    public static function getDefaultNrOfItemsPerPage()
    {
        $oConfig = Registry::getConfig();
        $sListType = $oConfig->getConfigParam('sDefaultListDisplayType');
        $aNrOfItemsPerPage = (
            ('grid' === $sListType) ?
            $oConfig->getConfigParam('aNrofCatArticlesInGrid') :
            $oConfig->getConfigParam('aNrofCatArticles')
        );
        if (is_array($aNrOfItemsPerPage)) {
            $iNrOfItemsPerPage = (int)array_shift($aNrOfItemsPerPage);
        }
        $iNrOfItemsPerPage = ($iNrOfItemsPerPage == 0 ? 10 : $iNrOfItemsPerPage);

        return $iNrOfItemsPerPage;
    }

    /**
    * set Locale
    *
    * @return null
    */
    public static function setLocale()
    {
        $oLang = Registry::getLang();
        $sLocaleCode = $oLang->getLanguageAbbr() . '_' . Str::getStr()->strtoupper($oLang->getLanguageAbbr());
        setlocale(LC_ALL, $sLocaleCode);
    }

    /**
    * add a Value to the Constraint of a Module Option select
    *
    * @param string $sCfgVarname - name of variable
    * @param string $sCfgModule  - optional name of module (e.g. 'module:bsimport')
    * @param string $sOption
    *
    * @return boolean
    */
    public static function addOptionToShopConfigConstraint($sCfgVarname = '', $sCfgModule = '', $sOption = '')
    {
        if (!self::isDBConfig() && $sCfgVarname && $sCfgModule && $sOption) {
            $oModuleConfigurationDaoBridge = ContainerFactory::getInstance()->getContainer()->get(ModuleConfigurationDaoBridgeInterface::class);
            $oModuleConfiguration = $oModuleConfigurationDaoBridge->get($sCfgModule);
            $oModuleSetting = $oModuleConfiguration->getModuleSetting($sCfgVarname);
            $aContraints = $oModuleSetting->getConstraints();
            if (!in_array($sOption, $aContraints))
            {
                $aContraints[] = $sOption;
                $oModuleSetting->setConstraints($aContraints);
                $oModuleConfigurationDaoBridge->save($oModuleConfiguration);
            }
        }

        return self::addOptionToDBConfigConstraint($sCfgVarname, $sCfgModule, $sOption);
    }

    /**
    * add a Value to the Constraint of a DB Module Option select (OXID < v6.2)
    *
    * @param string $sCfgVarname - name of variable
    * @param string $sCfgModule  - optional name of module (e.g. 'bsimport')
    * @param string $sOption
    *
    * @return boolean
    */
    public static function addOptionToDBConfigConstraint($sCfgVarname = '', $sCfgModule = '', $sOption = '')
    {
        $bResult = false;

        if ($sCfgVarname && $sCfgModule && $sOption) {
            $aConstraint = self::getDBConfigConstraint($sCfgVarname, $sCfgModule, false);
            if (!is_array($aConstraint)) {
                $aConstraint = [];
            }
            if (!in_array($sOption, $aConstraint)) {
                $aConstraint[] = $sOption;
            }
            $bResult = self::setDBConfigConstraint($sCfgVarname, $sCfgModule, $aConstraint);
        }
        return $bResult;
    }

    /**
    * add a Value to a array Module Option
    *
    * @param string $sCfgVarname - name of variable
    * @param string $sCfgModule  - optional name of module (e.g. 'bsimport')
    * @param array  $aOption     - array with options
    * @param string $sArrayType  - the type of Config array (e.g. 'arr', 'aarr')
    *
    * @return boolean
    */
    public static function addOptionToShopConfigArray($sCfgVarname = '', $sCfgModule = '', $aOptions = [], $sArrayType = 'arr')
    {
        if (!self::isDBConfig() && $sCfgModule) {
            $oModuleConfigurationDaoBridge = ContainerFactory::getInstance()->getContainer()->get(ModuleConfigurationDaoBridgeInterface::class);
            $oModuleConfiguration = $oModuleConfigurationDaoBridge->get($sCfgModule);
            $oModuleSetting = $oModuleConfiguration->getModuleSetting($sCfgVarname);
            $aConfig = self::_addOptionsToArray($oModuleSetting->getValue(), $aOptions, $sArrayType);

            $oModuleSetting->setValue($aConfig);
            $oModuleConfigurationDaoBridge->save($oModuleConfiguration);
        }

        return self::addOptionToDBConfigArray($sCfgVarname, $sCfgModule, $aOptions, $sArrayType);
    }

    /**
    * add a Value to a array Module DB Option (OXID < v6.2)
    *
    * @param string $sCfgVarname - name of variable
    * @param string $sCfgModule  - optional name of module (e.g. 'bsimport')
    * @param array  $aOption     - array with options
    * @param string $sArrayType  - the type of Config array (e.g. 'arr', 'aarr')
    *
    * @return boolean
    */
    public static function addOptionToDBConfigArray($sCfgVarname = '', $sCfgModule = '', $aOptions = [], $sArrayType = 'arr')
    {
        $bResult = false;

        if ($sCfgVarname && $aOptions && in_array($sArrayType, ['arr', 'aarr'])) {
            $oConfig = Registry::getConfig();
            $aConfig = $oConfig->getConfigParam($sCfgVarname);
            if (!is_array($aConfig)) {
                $aConfig = [];
            }

            $aConfig = self::_addOptionsToArray($aConfig, $aOptions, $sArrayType);

            self::saveConfigParam($sCfgVarname, $aConfig, $sArrayType, $sCfgModule);

            $bResult = true;
        }
        return $bResult;
    }

    /**
    * remove a Value from the Constraint of a Module Option select
    *
    * @param string $sCfgVarname - name of variable
    * @param string $sCfgModule  - optional name of module (e.g. 'bsimport')
    * @param string $sOption
    *
    * @return boolean
    */
    public static function removeOptionFromShopConfigConstraint($sCfgVarname = '', $sCfgModule = '', $sOption = '')
    {
        if (self::isDBConfig()) {
            return self::removeOptionFromDBConfigConstraint($sCfgVarname, $sCfgModule, $sOption);
        }

        $bResult = false;

        if ($sCfgVarname && $sCfgModule && $sOption) {
            $oModuleConfigurationDaoBridge = ContainerFactory::getInstance()->getContainer()->get(ModuleConfigurationDaoBridgeInterface::class);
            $oModuleConfiguration = $oModuleConfigurationDaoBridge->get($sCfgModule);
            $oModuleSetting = $oModuleConfiguration->getModuleSetting($sCfgVarname);
            $aContraints = $oModuleSetting->getConstraints();
            $aContraints = array_diff($aContraints, [$sOption]);
            $oModuleSetting->setConstraints($aContraints);
            $bResult = $oModuleConfigurationDaoBridge->save($oModuleConfiguration);
        }
        return $bResult;
    }

    /**
    * remove a Value from the Constraint of a Module DB-Option select (OXID < v6.2)
    *
    * @param string $sCfgVarname - name of variable
    * @param string $sCfgModule  - optional name of module (e.g. 'bsimport')
    * @param string $sOption
    *
    * @return boolean
    */
    public static function removeOptionFromDBConfigConstraint($sCfgVarname = '', $sCfgModule = '', $sOption = '')
    {
        $bResult = false;

        if ($sOption) {
            if ($aConstraint = self::getDBConfigConstraint($sCfgVarname, $sCfgModule, false)) {
                if (($iKey = array_search($sOption, $aConstraint)) !== false) {
                    unset($aConstraint[$iKey]);
                }
                self::setDBConfigConstraint($sCfgVarname, 'module:' . $sCfgModule, $aConstraint);
                $bResult = true;
            }
        }
        return $bResult;
    }

    /**
    * remove a Value from the Constraint of a Theme Option select
    *
    * @param string $sCfgVarname - name of variable
    * @param string $sCfgModule  - optional name of module (e.g. 'bsimport')
    * @param string $sOption
    *
    * @return boolean
    */
    public static function removeOptionFromShopConfigThemeConstraint($sCfgVarname = '', $sCfgModule = '', $sOption = '')
    {
        return self::removeOptionFromDBConfigThemeConstraint($sCfgVarname, $sCfgModule, $sOption);
    }

    /**
    * remove a Value from the Constraint of a Theme DB-Option select (OXID < v6.2)
    *
    * @param string $sCfgVarname - name of variable
    * @param string $sCfgModule  - optional name of module (e.g. 'bsimport')
    * @param string $sOption
    *
    * @return boolean
    */
    public static function removeOptionFromDBConfigThemeConstraint($sCfgVarname = '', $sCfgModule = '', $sOption = '')
    {
        $bResult = false;

        if ($sOption) {
            if ($aConstraint = self::getDBConfigConstraint($sCfgVarname, $sCfgModule, false)) {
                if (($iKey = array_search($sOption, $aConstraint)) !== false) {
                    unset($aConstraint[$iKey]);
                }
                self::setDBConfigConstraint($sCfgVarname, 'theme:' . $sCfgModule, $aConstraint);
                $bResult = true;
            }
        }
        return $bResult;
    }

    /**
    * remove a Value from a array Module Option
    *
    * @param string $sCfgVarname - name of variable
    * @param string $sCfgModule  - optional name of module (e.g. 'bsimport')
    * @param string $sOption     - if we have an "arr" $sOption is the value, if we have an "aarr" $sOption is the Key or the value
    * @param string $sArrayType  - the type of Config array (e.g. 'arr', 'aarr')
    *
    * @return boolean
    */
    public static function removeOptionFromShopConfigArray($sCfgVarname = '', $sCfgModule = '', $sOption = '', $sArrayType = 'arr')
    {
        // there is no better way for OXID 6.2, so we use the old method too
        return self::removeOptionFromDBConfigArray($sCfgVarname, $sCfgModule, $sOption, $sArrayType);
    }

    /**
    * remove a Value from a array Module DB-Option (OXID < v6.2)
    *
    * @param string $sCfgVarname - name of variable
    * @param string $sCfgModule  - optional name of module (e.g. 'bsimport')
    * @param string $sOption     - if we have an "arr" $sOption is the value, if we have an "aarr" $sOption is the Key or the value
    * @param string $sArrayType  - the type of Config array (e.g. 'arr', 'aarr')
    *
    * @return boolean
    */
    public static function removeOptionFromDBConfigArray($sCfgVarname = '', $sCfgModule = '', $sOption = '', $sArrayType = 'arr')
    {
        $bResult = false;

        if ($sOption && in_array($sArrayType, ['arr', 'aarr'])) {
            $oConfig = Registry::getConfig();
            $aConfig = $oConfig->getConfigParam($sCfgVarname);

            if ($sKey = ($sArrayType == 'aarr') ? $sOption : array_search($sOption, $aConfig)) {
                unset($aConfig[$sKey]);
                self::saveConfigParam($sCfgVarname, $aConfig, $sArrayType, $sCfgModule);
            }
            $bResult = true;
        }
        return $bResult;
    }

    /**
    * get the Constraint of a Module Option
    *
    * @param string $sCfgVarname - name of variable
    * @param string $sCfgModule  - optional name of module (e.g. 'bsimport')
    * @param string $bAsString   - result as string or array
    *
    * @return mixed (array, string, boolean)
    */
    public static function getShopConfigConstraint($sCfgVarname = '', $sCfgModule = '', $bAsString = true)
    {
        if (self::isDBConfig() || (stripos($sCfgModule, 'theme:') !== false)) {
            return self::getDBConfigConstraint($sCfgVarname, $sCfgModule, $bAsString);
        }

        $mResult = false;

        $oModuleConfigurationDaoBridge = ContainerFactory::getInstance()->getContainer()->get(ModuleConfigurationDaoBridgeInterface::class);
        $oModuleConfiguration = $oModuleConfigurationDaoBridge->get($sCfgModule);
        $oModuleSetting = $oModuleConfiguration->getModuleSetting($sCfgVarname);
        $mResult = $oModuleSetting->getConstraints();
        if ($bAsString) {
            $mResult = implode('|', $mResult);
        }
        return $mResult;
    }

    /**
    * get the Constraint of a Module DB-Option (OXID < v6.2)
    *
    * @param string $sCfgVarname - name of variable
    * @param string $sCfgModule  - optional name of module (e.g. 'bsimport')
    * @param string $bAsString   - result as string or array
    *
    * @return mixed (array, string, boolean)
    */
    public static function getDBConfigConstraint($sCfgVarname = '', $sCfgModule = '', $bAsString = true)
    {
        $mResult = false;
        if ($sCfgVarname) {
            $aCheck = [
                'oxcfgvarname' => $sCfgVarname
            ];

            if ($sCfgModule) {
                $aCheck['oxcfgmodule'] = $sCfgModule;
            }

            if (
                $sConstraint = ToolsDB::getAnyId(
                    'oxconfigdisplay',
                    $aCheck,
                    'oxvarconstraint'
                )
            ) {
                $aConstraint = array_filter(explode('|', $sConstraint));

                if ($bAsString) {
                    $mResult = implode('|', $aConstraint);
                } else {
                    $mResult = $aConstraint;
                }
            }
        }

        return $mResult;
    }

    /**
    * set the Constraint of a Module-Config
    *
    * @param string $sCfgVarname - name of variable
    * @param string $sCfgModule  - optional name of module (e.g. 'bsimport')
    * @param mixed  $mConstraint - value as array or string
    *
    * @return boolean
    */
    public static function setShopConfigConstraint($sCfgVarname = '', $sCfgModule = '', $mConstraint = null)
    {
        if (self::isDBConfig() || (stripos($sCfgModule, 'theme:') !== false)) {
            return self::setDBConfigConstraint($sCfgVarname, $sCfgModule, $mConstraint);
        }

        $bResult = false;

        if ($sCfgVarname && $sCfgModule && $mConstraint) {
            if (is_string($mConstraint) && stripos($mConstraint, '|') !== false) {
                $mConstraint = explode('|', $mConstraint);
            }
            if (is_array($mConstraint)) {

                $oModuleConfigurationDaoBridge = ContainerFactory::getInstance()->getContainer()->get(ModuleConfigurationDaoBridgeInterface::class);
                $oModuleConfiguration = $oModuleConfigurationDaoBridge->get($sCfgModule);
                $oModuleSetting = $oModuleConfiguration->getModuleSetting($sCfgVarname);
                $oModuleSetting->setConstraints($mConstraint);
                $bResult = $oModuleConfigurationDaoBridge->save($oModuleConfiguration);
            }
        }
        return $bResult;
    }

    /**
    * set the Constraint of a DB-Module-Config (OXID < v6.2)
    *
    * @param string $sCfgVarname - name of variable
    * @param string $sCfgModule  - optional name of module (e.g. 'bsimport')
    * @param mixed  $mConstraint - value as array or string
    *
    * @return boolean
    */
    public static function setDBConfigConstraint($sCfgVarname = '', $sCfgModule = '', $mConstraint = null)
    {
        $bResult = false;

        if ($sCfgVarname && $sCfgModule) {
            if (is_array($mConstraint)) {
                $mConstraint = implode('|', $mConstraint);
            }

            if ($sCfgModule && !strpos($sCfgModule, ':')) {
                $sCfgModule = 'module:' . $sCfgModule;
            }

            $oDb = DatabaseProvider::getDb();
            $sSql = "update `oxconfigdisplay`
                set `oxvarconstraint` = " . $oDb->quote($mConstraint) . "
                where `oxcfgvarname` = " . $oDb->quote($sCfgVarname) . "
                and `oxcfgmodule` = " . $oDb->quote($sCfgModule);

            $bResult = ToolsDB::execute($sSql);
        }
        return $bResult;
    }

    /**
    * set the Module-Definer of a Module-Config
    *
    * @param string $sCfgModule   - name of module-definer (e.g. 'trwexport')
    * @param string $sCfgVarname  - name of variable
    * @param string $sCfgGroup    - name of group
    * @param string $sCfgType     - name of type
    * @param string $aConstraint  - possible Constraints
    * @param string $mValue       - possible Value
    * @param int    $iShopId      - ShopId
    *
    * @return boolean
    */
    public static function createShopConfig($sCfgModule = '', $sCfgVarname = '', $sCfgGroup = '', $sCfgType = 'str', $aConstraint = [], $mValue = null, $iShopId = null)
    {
        if (self::isDBConfig()) {
            return self::createDBConfig($sCfgModule, $sCfgVarname, $sCfgGroup, $sCfgType, $aConstraint, $mValue, $iShopId);
        }

        $bResult = false;

        if ($sCfgModule && $sCfgGroup && $sCfgVarname && in_array($sCfgType, self::$_aPossibleOptionTypes)) {

            $oModuleConfigurationDaoBridge = ContainerFactory::getInstance()->getContainer()->get(ModuleConfigurationDaoBridgeInterface::class);
            if ($oModuleConfiguration = $oModuleConfigurationDaoBridge->get($sCfgModule)) {
                if (!$oModuleConfiguration->hasModuleSetting($sCfgVarname)) {
                    $oModuleSetting = new Setting();
                    $oModuleSetting->setName($sCfgVarname);
                    $oModuleSetting->setType($sCfgType);
                    $oModuleSetting->setGroupName($sCfgGroup);
                    if (count($aConstraint)) {
                        $oModuleSetting->setConstraints($aConstraint);
                    }
                    if (!is_null($mValue)) {
                        $oModuleSetting->setValue($mValue);
                    }

                    $oModuleConfiguration->addModuleSetting($oModuleSetting);
                    $bResult = $oModuleConfigurationDaoBridge->save($oModuleConfiguration);
                }
            }
        }
        return $bResult;
    }

    /**
    * set the Module-Definer of a Module-Config
    *
    * @param string $sCfgModule   - name of module-definer (e.g. 'trwexport')
    * @param string $sCfgVarname  - name of variable
    * @param string $sCfgGroup    - name of group
    * @param string $sCfgType     - name of type
    * @param string $mConstraint  - possible Constraints
    * @param string $mValue       - possible Value
    * @param int    $iShopId      - ShopId
    *
    * @return boolean
    */
    public static function createDBConfig($sCfgModule = '', $sCfgVarname = '', $sCfgGroup = '', $sCfgType = 'str', $mConstraint = null, $mValue = null, $iShopId = null)
    {
        $bResult = false;

        if ($sCfgModule && !strpos($sCfgModule, ':')) {
            $sCfgModule = 'module:' . $sCfgModule;
        }

        $sCheckOxId = ToolsDB::getAnyId(
            'oxconfig',
            [
                'oxshopid'  => $iShopId,
                'oxvarname' => $sCfgVarname,
                'oxmodule'  => $sCfgModule
            ]
        );

        if (
            $sCfgModule &&
            $sCfgGroup &&
            $sCfgVarname &&
            in_array($sCfgType, self::$_aPossibleOptionTypes) &&
            !$sCheckOxId
        ) {
            $bResult = true;

            $sConstraint = '';
            if ($mConstraint) {
                $sConstraint = (is_array($mConstraint) ? implode('|', $mConstraint) : $mConstraint);
            }
            $sOxId = Registry::getUtilsObject()->generateUId();

            self::saveConfigParam($sCfgVarname, $mValue, $sCfgType, $sCfgModule, $iShopId);

            $oDb = DatabaseProvider::getDb();

            $sSql = "delete from `oxconfigdisplay`
                where `oxcfgmodule` = " . $oDb->quote($sCfgModule). "
                and `oxcfgvarname` = " . $oDb->quote($sCfgVarname);
            ToolsDB::execute($sSql);

            $sSql = "insert into `oxconfigdisplay`
                (`OXID`, `OXCFGMODULE`, `OXCFGVARNAME`, `OXGROUPING`, `OXVARCONSTRAINT`)
                VALUES
                (" .
                    $oDb->quote($sOxId) . "," .
                    $oDb->quote($sCfgModule) . "," .
                    $oDb->quote($sCfgVarname) . ", " .
                    $oDb->quote($sCfgGroup) . ", " .
                    $oDb->quote($sConstraint) . "
                )";
            ToolsDB::execute($sSql);
        }
        return $bResult;

    }

    /**
    * set the Module-Definer of a Module-Config
    *
    * @param string $sCfgModule   - name of module-definer (e.g. 'trwexport')
    * @param string $sCfgVarname  - name of variable
    * @param string $sCfgGroup    - name of group
    * @param string $sCfgType     - name of type
    * @param string $aConstraint  - possible Constraints
    * @param string $mValue       - possible Value
    * @param int    $iShopId      - ShopId
    *
    * @return boolean
    */
    public static function updateDBConfig($sCfgModule = '', $sCfgVarname = '', $sCfgGroup = '', $sCfgType = 'str', $aConstraint = [], $mValue = null, $iShopId = null)
    {
        $bResult = false;

        if ($sCfgModule && !strpos($sCfgModule, ':')) {
            $sCfgModule = 'module:' . $sCfgModule;
        }

        $sCheckOxId = ToolsDB::getAnyId(
            'oxconfig',
            [
                'oxshopid'  => $iShopId,
                'oxvarname' => $sCfgVarname,
                'oxmodule'  => $sCfgModule
            ]
        );

        if (
            $sCfgModule &&
            $sCfgGroup &&
            $sCfgVarname &&
            in_array($sCfgType, self::$_aPossibleOptionTypes) &&
            $sCheckOxId
        ) {
            $bResult = self::saveConfigParam($sCfgVarname, $mValue, $sCfgType, $sCfgModule, $iShopId);
        }
        else {
            $bResult = self::createDBConfig($sCfgModule, $sCfgVarname, $sCfgGroup, $sCfgType, $aConstraint, $mValue, $iShopId);
        }
        return $bResult;
    }

    /**
    * rename the Module-Group of a Module-Config
    *
    * @param string $sOldName - name of group
    * @param string $sNewName - new name of group
    * @param string $sThemeId
    *
    * @return boolean
    */
    public static function renameShopConfigThemeGroup($sOldName = '', $sNewName = '', $sThemeId = '')
    {
        return self::renameDBConfigThemeGroup($sOldName, $sNewName, $sThemeId);
    }

    /**
    * rename the Module-Group of a Module-DBConfig (OXID < v6.2)
    *
    * @param string $sOldName - name of group
    * @param string $sNewName - new name of group
    * @param string $sThemeId
    *
    * @return boolean
    */
    public static function renameDBConfigThemeGroup($sOldName = '', $sNewName = '', $sThemeId = '')
    {
        $bResult = false;

        if ($sOldName && $sNewName && $sThemeId) {
            $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
            $sSql = "update `oxconfigdisplay`
                set `oxgrouping` = " . $oDb->quote($sNewName) . "
                where `oxcfgmodule` = " . $oDb->quote('theme:' . $sThemeId) . "
                and `oxgrouping` = " . $oDb->quote($sOldName);
            $bResult = ToolsDB::execute($sSql);
        }
        return $bResult;
    }

    /**
    * check if it is a MultiShop-System
    *
    * @return boolean
    */
    public static function isMultiShop()
    {
        return !('CE' == Registry::getConfig()->getEdition());
    }

    /**
    * get List of shopIds
    *
    * @return array
    */
    public function getShopIds()
    {
        $oConfig = Registry::getConfig();

        $aShopIds = [];
        if (!self::isMultiShop()) {
            $aShopIds[] = $oConfig->getShopId();
        } else {
            $aShopIds = $oConfig->getShopIds();
        }
        return $aShopIds;
    }

    /**
    * check if config save in DB or newer
    *
    * @return boolean
    */
    public static function isDBConfig()
    {
        return version_compare(ShopVersion::getVersion(), '6.2.0', '<');
    }

    /**
    * get ShopConfigValues from Config
    *
    * @param array  $aConfigFields
    * @param bool   $bInclude if true include the fields, else exclude them.
    * @param int    $iShopId - ShopId
    * @param string $sModule - Config for module or theme ...
    *
    * @return array
    */
    public static function getShopConfigValues($aConfigFields = [], $bInclude = false, $iShopId = null, $sModule = '')
    {
        $oConfig = Registry::getConfig();
        $iShopId = $iShopId ?? $oConfig->getShopId();

        if ($sModule) {
            $sSql = "SELECT c.`oxvarname`, c.`oxvartype`, %s as oxvarvalue,
                d.`oxgrouping`, d.`oxvarconstraint`, d.`oxpos`
                FROM `oxconfig` as c
                LEFT JOIN `oxconfigdisplay` as d ON (c.`oxmodule` = d.`oxcfgmodule` and c.`oxvarname` = d.`oxcfgvarname`)
                WHERE c.`oxshopid` = '%s'
                AND c.`oxmodule` = " . DatabaseProvider::getDb()->quote($sModule) . "
                ORDER BY c.`oxvarname` ASC";
        }
        else {
            $sSql = "SELECT `oxvarname`, `oxvartype`, %s as oxvarvalue
                FROM `oxconfig`
                WHERE `oxshopid` = '%s'
                AND `oxmodule` = ''
                ORDER BY `oxvarname` ASC";
        }

        $sSql = sprintf(
            $sSql,
            $oConfig->getDecodeValueQuery(),
            $iShopId
        );

        $aConfigValues  = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC)->getAll($sSql);

        $aResult = [];
        foreach ($aConfigValues as $aConfigValue) {

            $sVarName  = $aConfigValue['oxvarname'];
            $sVarType  = $aConfigValue['oxvartype'];
            $mVarValue = $aConfigValue['oxvarvalue'];

            $bFound = false;
            foreach ($aConfigFields as $sConfigField) {
                if (stripos($sVarName, $sConfigField) !== false) {
                    $bFound = true;
                    break;
                }
            }
            if (($bInclude && !$bFound) || (!$bInclude && $bFound)) {
                continue;
            }

            if (in_array($sVarType, ['aarr', 'arr'])) {
                $mVarValue = unserialize($mVarValue);
                $mVarValue = is_array($mVarValue) ? $mVarValue : [];
            }
            elseif ($sVarType == 'bool') {
                $mVarValue = ($mVarValue ? true : false);
            }

            $aResult[$sVarName] = [
                'type'  => $sVarType,
                'value' => $mVarValue
            ];
            if ($sModule) {
                $aResult[$sVarName]['pos'] = (int)$aConfigValue['oxpos'];
                $aResult[$sVarName]['group'] = $aConfigValue['oxgrouping'];
                $aResult[$sVarName]['constraints'] = $aConfigValue['oxvarconstraint'];
            }
        }
        return $aResult;
    }

    /**
    * get ShopConfigValues from Config for Export
    *
    * @param array  $aExcludeConfigFields
    * @param int    $iShopId - ShopId
    * @param string $sModule - Config for module or theme ...
    *
    * @return array
    */
    public static function getShopConfigValuesForExport($aExcludeConfigFields = [], $iShopId = null, $sModule = '')
    {
        $aConfigFields = array_merge(self::$_aExcludeInternalShopConfigs, $aExcludeConfigFields);
        return self::getShopConfigValues(
            $aConfigFields,
            false,
            $iShopId,
            $sModule
        );
    }

    /**
    * get ShopConfigValues from Table for Export
    *
    * @param array $aExcludeConfigFields
    * @param int   $iShopId - ShopId
    *
    * @return array
    */
    public static function getShopTableValuesForExport($aExcludeConfigFields = [], $iShopId = null) {
        $oShop = oxNew(\OxidEsales\Eshop\Application\Model\Shop::class);
        $oShop->load($iShopId);

        $oTableDesc = DatabaseProvider::getInstance()->getTableDescription('oxshops');
        $aTableColDesc = [];
        foreach ($oTableDesc as $oTableColDesc) {
            $sName = Str::getStr()->strtolower($oTableColDesc->name);
            $sType = stripos($oTableColDesc->type, 'int') ? 'int' : 'str';
            $aTableColDesc[$sName] = $sType;
        }

        $aResult = [];
        $aExcludeConfigFields = array_merge($aExcludeConfigFields, self::$_aExcludeShopTableValues);
        $aMultiLingualFields = [];

        foreach ($oShop->getFieldNames() as $sFieldName) {
            if (in_array($sFieldName, $aExcludeConfigFields)) {
                continue;
            }
            $oField = $oShop->{"oxshops__" . $sFieldName};

            $aResult[$sFieldName]['type'] = $aTableColDesc[$sFieldName];

            // collect multiLangFields
            if ($oShop->isMultilingualField($sFieldName)) {
                $aMultiLingualFields[] = $sFieldName;
            }
            else {
                $aResult[$sFieldName]['value'] = $oField->getRawValue();
            }
        }

        foreach (Registry::getLang()->getLanguageIds() as $iLang => $sLangAbbr) {
            $oShop->loadInLang($iLang, $iShopId);
            foreach ($aMultiLingualFields as $sFieldName) {
                $oField = $oShop->{"oxshops__" . $sFieldName};
                $aResult[$sFieldName]['value'][$sLangAbbr] = $oField->getRawValue();
            }
        }

        return $aResult;
    }

    /**
    * add Values to a array
    *
    * @param string $aConfig    - array with Values
    * @param string $aOptions   - array with new vales
    * @param string $sArrayType - the type of Config array (e.g. 'arr', 'aarr')
    *
    * @return boolean
    */
    protected static function _addOptionsToArray($aConfig = [], $aOptions = [], $sArrayType = 'arr')
    {
        $aConfig = (is_array($aConfig) ? $aConfig : []);
        if (is_array($aOptions)) {
            foreach ($aOptions as $mKey => $sOption) {
                if (
                    $sArrayType == 'arr' &&
                    !in_array($sOption, $aConfig)
                ) {
                    $aConfig[] = $sOption;
                } elseif (
                    $sArrayType == 'aarr' &&
                    (
                        !array_key_exists($mKey, $aConfig) ||
                        (array_key_exists($mKey, $aConfig) && $aConfig !== $sOption)
                    )
                ) {
                    $aConfig[$mKey] = $sOption;
                }
            }
        }
        return $aConfig;
    }


    protected function _varValueWithTypeInfo($sVarName, $mVarValue, $sVarType)
    {
        if ($sVarType === 'aarr' && count($mVarValue) > 1) {
            //if array contains more than one item it can be distinguished from the assoc array we use for type
        } elseif ($sVarType === 'arr') {
            // arrays can be recognised
        } else {
            // default type
            $typeInfoNeeded = true;
            $boolPrefix = substr($sVarName, 0, 2) === "bl";

            if ($sVarType == 'str' && !$boolPrefix) {
                $typeInfoNeeded = false;
            }

            if ($sVarType == 'select' && !$boolPrefix) {
                $typeInfoNeeded = false;
            }

            if ($sVarType == 'bool' && $boolPrefix) {
                $typeInfoNeeded = false;
            }

            if ($sVarType == 'bool' && ($mVarValue === '1' || $mVarValue === '' || $mVarValue === 'true' || $mVarValue === 'false')) {
                $mVarValue = (bool) $mVarValue;
                $typeInfoNeeded = false;
            }

            if ($typeInfoNeeded) {
                $mVarValue = array($sVarType => $mVarValue);
            }
        }

        return $mVarValue;
    }
}
