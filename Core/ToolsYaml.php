<?php

/**
* @author    Mario Lorenz, www.the-real-world.de
* @copyright 2021 the-real-world.de
* @license   https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

namespace TheRealWorld\ToolsPlugin\Core;

use OxidEsales\Eshop\Core\Registry;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;

class ToolsYaml
{
    /**
    * return Yaml Content
    *
    * @param string $sYaml
    *
    * @return array
    */
    public static function getYamlContent($sYaml)
    {
        $aResult = [];
        if ($sYaml) {
            try {
                $aResult = Yaml::parse($sYaml);
            } catch (ParseException $exception) {
                sprintf('Unable to parse the YAML string: %s', $exception->getMessage());
            }
        }
        return $aResult;
    }

    /**
    * get Yaml File
    *
    * @param string $sYamlFile
    *
    * @return string
    */
    public static function getYamlStringFromFile($sYamlFile)
    {
        $sYaml = '';
        if (file_exists($sYamlFile)) {
            $sYaml = file_get_contents($sYamlFile);
        }
        return $sYaml;
    }

    /**
    * set Yaml File
    *
    * @param string $sYamlFile
    * @param array $aData
    *
    * @return boolean
    */
    public static function setDataToYamlFile($sYamlFile, $aData = [])
    {
        $bResult = false;
        if ($sYamlFile) {
            if ($sYaml = self::getYamlFromArray($aData)) {
                $bResult = file_put_contents(
                    $sYamlFile,
                    $sYaml
                );
            }
        }
        return $bResult;
    }

    /**
    * get Yaml From Array
    *
    * @param array $aData
    *
    * @return string
    */
    public static function getYamlFromArray($aData = [])
    {
        $sResult = '';
        if (is_array($aData)) {
            $sResult = Yaml::dump($aData, 5, 2);
        }
        return $sResult;
    }
}