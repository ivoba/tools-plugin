<?php

/**
* @author    Mario Lorenz, www.the-real-world.de
* @copyright 2021 the-real-world.de
* @license   https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

namespace TheRealWorld\ToolsPlugin\Core;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\TableViewNameGenerator;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;

class ToolsOxid
{
    /**
    * Returns string built from category titles
    *
    * @param \OxidEsales\Eshop\Application\Model\Category $oCat category object
    * @param string $sSep in Category Path
    *
    * @return string
    */
    public static function getCatPath(\OxidEsales\Eshop\Application\Model\Category $oCat, $sSep = ' / ')
    {
        $sCatPathString = '';
        $sTmpSep = '';
        while ($oCat) {
            // prepare oCat title part
            $sCatPathString = $oCat->oxcategories__oxtitle->value . $sTmpSep . $sCatPathString;
            $sTmpSep = $sSep;
            // load parent
            $oCat = $oCat->getParentCategory();
        }
        return $sCatPathString;
    }

    /**
    * set a Attribute Value
    *
    * @param string $sOxObjectId the oxid of article
    * @param string $sAttributeOxId the oxid of attribute
    * @param string $sValue the title
    * @param integer $iLang
    *
    * @return string sOxId
    */
    public static function setAttributeValue($sOxObjectId = null, $sAttributeOxId = null, $sValue = null, $iLangId = null)
    {
        if ($sAttributeOxId && $sValue) {
            $oObj2Attrib = oxNew(\OxidEsales\Eshop\Core\Model\MultiLanguageModel::class);
            $oObj2Attrib->init("oxobject2attribute");
            $oObj2Attrib->setLanguage($iLangId);

            $sAttribValueOxId = ToolsDB::getAnyId(
                'oxobject2attribute',
                [
                    'oxobjectid' => $sOxObjectId,
                    'oxattrid'   => $sAttributeOxId
                ]
            );

            if (!$sAttribValueOxId) {
                $sAttribValueOxId = Registry::getUtilsObject()->generateUID();
            }
            $oObj2Attrib->loadInLang($iLangId, $sAttribValueOxId);
            $oObj2Attrib->setId($sAttribValueOxId);
            $aParams = [
                'oxobjectid' => $sOxObjectId,
                'oxattrid'   => $sAttributeOxId,
                'oxvalue'    => $sValue,
            ];
            $aParams = ToolsDB::convertDB2OxParams($aParams, 'oxobject2attribute');
            $oObj2Attrib->assign($aParams);
            $oObj2Attrib->save();
        }
    }

    /**
    * Save Manufacturer
    *
    * @param string  - $sTitle
    * @param string  - $sDescription
    * @param integer - $iLangId
    *
    * @return string sOxidId
    */
    public static function setManufacturer($sTitle = null, $sDescription = null, $iLangId = null)
    {
        $sOxId = null;
        if ($sTitle) {
            $oManufacturer = oxNew(\OxidEsales\Eshop\Application\Model\Manufacturer::class);
            $oManufacturer->setLanguage($iLangId);

            if ($sOxId = ToolsDB::getAnyId('oxmanufacturers', ['oxtitle' => $sTitle])) {
                $oManufacturer->load($sOxId);
                if ($iLangId) {
                    $oManufacturer->loadInLang($iLangId, $sOxId);
                }
            }

            $aParams = [
                'oxtitle'     => $sTitle,
                'oxshortdesc' => $sDescription
            ];
            $aParams = ToolsDB::convertDB2OxParams($aParams, 'oxmanufacturers');

            $oManufacturer->assign($aParams);
            $oManufacturer->save();
            $sOxId = $oManufacturer->getId();
        }
        return $sOxId;
    }


    /**
    * Save link between Article and category
    *
    * @param string $sOxObjectId
    * @param string $sOxCatnId
    *
    * @return string sOxidId
    */
    public static function setArticleToCategory($sOxObjectId = null, $sOxCatnId = null)
    {
        $sOxId = null;

        if ($sOxObjectId && $sOxCatnId) {
            $sOxId = ToolsDB::getAnyId('oxobject2category', ['oxobjectid' => $sOxObjectId, 'oxcatnid' => $sOxCatnId]);
            $oOxObject2Category = oxNew(\OxidEsales\Eshop\Core\Model\BaseModel::class);
            $oOxObject2Category->init("oxobject2category");

            if ($sOxId) {
                $oOxObject2Category->load($sOxId);
            }

            $aParams = [
                'oxobjectid' => $sOxObjectId,
                'oxcatnid'   => $sOxCatnId,
            ];
            $aParams = ToolsDB::convertDB2OxParams($aParams, 'oxobject2category');
            $oOxObject2Category->assign($aParams);
            $oOxObject2Category->save();
            $sOxId = $oOxObject2Category->getId();
            unset($oOxObject2Category);
        }
        return $sOxId;
    }

    /**
    * set a CategoryTitle
    *
    * @param string $sTitle the title
    * @param string $sCategoriesId the possible Id of the category
    * @param string $sParentId the Parent OxId
    * @param string $sRootOxCatId the Root OxId
    * @param integer $iLang
    *
    * @return string sOxId
    */
    public static function setCategoryTitle($sTitle = null, $sCategoriesId = null, $sParentId = null, $sRootOxCatId = null, $iLangId = null)
    {
        $sResult = null;
        if ($sTitle) {
            if (!$sCategoriesId) {
                $oDb = DatabaseProvider::getDb();
                $viewNameGenerator = Registry::get(TableViewNameGenerator::class);
                $sCatViewName = $viewNameGenerator->getViewName('oxcategories', $iLangId);
                $sSelect = "select `oxid`
                    from {$sCatViewName}
                    where lower(`oxtitle`) = " . $oDb->quote(Str::getStr()->strtolower($sTitle));
                $sCategoriesId = $oDb->getOne($sSelect);
            }

            if (!$sCategoriesId) {
                $oCategory = oxNew(\OxidEsales\Eshop\Application\Model\Category::class);
                $oCategory->setLanguage($iLangId);

                // if it is new, we set some additional informations
                $aParams = [
                    'oxactive'   => 1,
                    'oxtitle'    => $sTitle,
                    'oxparentid' => $sParentId,
                    'oxrootid'   => $sRootOxCatId
                ];
                $aParams = ToolsDB::convertDB2OxParams($aParams, 'oxcategories');
                $oCategory->assign($aParams);
                $bool = $oCategory->save();
                $sCategoriesId = $oCategory->getId();
            }

            $sResult = $sCategoriesId;
        }
        return $sResult;
    }
}
