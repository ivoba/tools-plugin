<?php

/**
* @author    Mario Lorenz, www.the-real-world.de
* @copyright 2021 the-real-world.de
* @license   https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

namespace TheRealWorld\ToolsPlugin\Core;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;
use TheRealWorld\ToolsPlugin\Core\ToolsLog;
use TheRealWorld\ToolsPlugin\Core\ToolsLang;

class ToolsApi
{
    /**
    * array with possible CUrl Request Types
    * @param array
    */
    protected static $_aPossibleCurlRequestTypes = ['POST', 'GET', 'DELETE'];

    /**
    * the default cUrl Options
    * @param array
    */
    protected static $_aCUrlDefaults = [
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_TIMEOUT       => 5
    ];

    /**
    * the CurlAttempts
    * @param integer
    */
    protected static $_iCurlAttempts = 3;

    /**
    * the Debug Log
    * @param array
    */
    protected static $_aDebugLog = [];

    /**
    * Connect to Webservice via cUrl
    *
    * @param string $sAPIUrl           - The Url of the extern API
    * @param array  $mRequestData      - Request-Parameters as Array or JSON-String
    * @param array  $aLoginData        - Array with authentication Data - we have the following possibilities
    *   - Basic User/Password -> ['basic' => ['user' => 'xxx', 'password' => 'yyy']]
    *   - Bearer Token        -> ['bearertoken' => 'xxx']
    *   - Base64 Token        -> ['base64token' => 'xxx'] - a token without exact authorization method description,
    *                                                            only "authorization"
    * @param array  $aOverwriteCUrlDefaults - POST, GET (default)
    * @param string $sTargetFile       - if we need to save the result in a file, the filename (+ Path)
    * @param string $aCurlHeaders      - Array with optional cUrl Headers
    * @param string $bFallbackSSLError - if we got no result with an SSL API Url, try i a second time with non SSL Url
    * @param string $bDebug            - DebugModus?
    *
    * @return mixed (string or boolean)
    */
    public static function executeExternApiCurl($sAPIUrl = '', $mRequestData = null, $aLoginData = [], $aOverwriteCUrlDefaults = [], $sTargetFile = '', $aCurlHeaders = [], $bFallbackSSLError = true, $bDebug = false)
    {
        $mResult = false;
        $oStr = Str::getStr();

        if ($sAPIUrl && !is_null($mRequestData)) {

            // cUrl Basics
            $aCurlOptions = self::$_aCUrlDefaults;

            // overwrite cUrl Defaults
            if (is_array($aOverwriteCUrlDefaults) && count($aOverwriteCUrlDefaults)) {
                foreach ($aOverwriteCUrlDefaults as $iCUrlKey => $mCUrlValue) {
                    // check RequestType
                    if ($iCUrlKey == CURLOPT_CUSTOMREQUEST) {
                        $mCUrlValue = (
                            in_array($mCUrlValue, self::$_aPossibleCurlRequestTypes) ?
                            $mCUrlValue :
                            self::$_aCUrlDefaults[CURLOPT_CUSTOMREQUEST]
                        );
                    }
                    $aCurlOptions[$iCUrlKey] = $mCUrlValue;
                }
            }

            //sanitize curl Header
            if (!is_array($aCurlHeaders)) {
                $aCurlHeaders = [];
            }

            // possible Login
            if (is_array($aLoginData)) {
                // Basic Authorization
                if (
                    isset($aLoginData['basic']['user']) &&
                    isset($aLoginData['basic']['password'])
                ) {
                    $aCurlOptions[CURLOPT_USERPWD] = $aLoginData['basic']['user'] . ':' . $aLoginData['basic']['password'];
                }

                // possible Bearer Token
                elseif (isset($aLoginData['bearertoken'])) {
                    $aCurlHeaders[] = 'Authorization: Bearer ' . $aLoginData['bearertoken'];
                }

                // possible Base64 Token without exact authorization method description
                elseif (isset($aLoginData['base64token'])) {
                    $aCurlHeaders[] = 'Authorization: ' . $aLoginData['base64token'];
                }
            }

            // cUrl Headers
            if (count($aCurlHeaders)) {
                $aCurlOptions[CURLOPT_HTTPHEADER] = $aCurlHeaders;
            }

            // cUrl RequestData
            if (is_array($mRequestData) && count($mRequestData)) {
                $mRequestData = http_build_query($mRequestData);
            }

            if ($aCurlOptions[CURLOPT_CUSTOMREQUEST] == 'POST') {
                $aCurlOptions[CURLOPT_POST] = true;
                $aCurlOptions[CURLOPT_POSTFIELDS] = $mRequestData;
            } else {
                $sAPIUrl .= '?' . $mRequestData;
            }

            // If defined a target file, we wrote the result in the file
            // open possible file Handle
            if (is_string($sTargetFile) && !empty($sTargetFile)) {
                $oFileHandle = fopen($sTargetFile, "w");
                $aCurlOptions[CURLOPT_FILE] = $oFileHandle;
                if (!ini_get('safe_mode') && !ini_get('open_basedir')) {
                    $aCurlOptions[CURLOPT_FOLLOWLOCATION] = true;
                }
            } else {
                $aCurlOptions[CURLOPT_RETURNTRANSFER] = true;
            }

            // Api-Url
            $aCurlOptions[CURLOPT_URL] = $sAPIUrl;

            $iAttempt = 0;
            do {
                if ($iAttempt > 0 && $bFallbackSSLError && (stripos($sAPIUrl, 'https') !== false)) {
                    self::setDebugLogEntry([
                        'ApiErrorCurlSSL' => sprintf(
                            ToolsLang::translateString(
                                'API cURL has Problems with SSL in %s, second try without https',
                                'TRWTOOLSPLUGIN'
                            ),
                            $sAPIUrl
                        )
                    ]);

                    $sAPIUrl = str_replace('https', 'http', $sAPIUrl);
                    $aCurlOptions[CURLOPT_URL] = $sAPIUrl;
                }

                // Run the cUrl
                $oCurlHandle = curl_init();

                curl_setopt_array($oCurlHandle, $aCurlOptions);

                $mResult = curl_exec($oCurlHandle);

                // simple Errorcodehandling
                $httpCode = (int)curl_getinfo($oCurlHandle, CURLINFO_HTTP_CODE);
                if ($httpCode != 200) {
                    $mResult = false;
                }

                // possible debugging
                if ($bDebug) {
                    self::setDebugLogEntry([
                        'ApiCurlHttpStatus' => $httpCode,
                        'ApiCurlHeader'     => $aCurlHeaders,
                        'ApiCurlOptions'    => $aCurlOptions,
                        'ApiCurlUrl'        => $sAPIUrl,
                        'ApiCurlResult'     => (empty($sTargetFile) ? $oStr->htmlentities($mResult) : $sTargetFile)
                    ]);
                }

                $iAttempt++;
            } while (!$mResult && $iAttempt < self::$_iCurlAttempts);

            // close possible file Handle
            if (!empty($sTargetFile) && file_exists($sTargetFile)) {
                fclose($oFileHandle);
                if (filesize($sTargetFile) > 0) {
                    $mResult = true;
                } else {
                    $mResult = false;
                    unlink($sTargetFile);
                }
            }

            curl_close($oCurlHandle);
        }
        elseif ($bDebug) {
            self::setDebugLogEntry([
                'ApiErrorURLOrRequestEmpty' => sprintf(
                    ToolsLang::translateString(
                        'API Url or Request Data Empty',
                        'TRWTOOLSPLUGIN'
                    )
                )
            ]);
        }

        return $mResult;
    }

    /**
    * Connect to Webservice via simple file_get_contents
    *
    * @param string $sAPIUrl      - The Url of the extern API
    * @param array  $aRequestData - Array with Request-Parameters
    * @param array  $aLoginData   - Array with authentication Data - for basic authentication
    *   - Basic User/Password -> ['basic' => ['user' => 'xxx', 'password' => 'yyy']]
    *   - Bearer Token        -> ['bearertoken' => 'xxx']
    *   - Base64 Token        -> ['base64token' => 'xxx'] - a token without exact authorization method description,
    *                                                            only "authorization"
    * @param string $sTargetFile  - if we need to save the result in a file, the filename (+ Path)
    * @param string $bDebug       - DebugModus?
    *
    * @return mixed (string or boolean)
    */
    public static function executeExternApiFileGetContents($sAPIUrl = '', $aRequestData = [], $aLoginData = [], $sTargetFile = '', $bDebug = false)
    {
        $mResult = false;

        if ($sAPIUrl) {
            if (is_array($aRequestData) && count($aRequestData)) {
                $sRequestData = http_build_query($aRequestData);
                $sAPIUrl .= '?' . $sRequestData;
            }

            if ($bDebug) {
                self::setDebugLogEntry([
                    'ApiFileGetContentsUrl'         => $sAPIUrl,
                    'ApiFileGetContentsLoginData'   => $aLoginData,
                    'ApiFileGetContentsRequestData' => $aRequestData
                ]);
            }

            $aContext = [];

            // possible Login
            if (is_array($aLoginData)) {
                $sAuth = '';

                // Basic Authorization
                if (
                    isset($aLoginData['basic']['user']) &&
                    isset($aLoginData['basic']['password'])
                ) {
                    $sAuth = 'Basic ' . base64_encode(
                        $aLoginData['basic']['user'] . ':' . $aLoginData['basic']['password']
                    );
                }

                // possible Bearer Token
                elseif (isset($aLoginData['bearertoken'])) {
                    $sAuth = 'Bearer ' . $aLoginData['bearertoken'];
                }

                // possible Base64 Token without exact authorization method description
                elseif (isset($aLoginData['base64token'])) {
                    $sAuth = $aLoginData['base64token'];
                }

                if ($sAuth) {
                    $aContext['http'] = [
                        'method' => 'GET',
                        'header' => 'Authorization: ' . $sAuth
                    ];
                }
            }

            $aResult = self::_fileGetContentsWithOptions($sAPIUrl, $aContext);

            // try it with some SSL ContextOptions
            if (isset($aResult['error'])) {
                if (stripos($sAPIUrl, 'https') !== false) {
                    if ($bDebug) {
                        self::setDebugLogEntry([
                            'ApiErrorFileGetContentsSSL' => sprintf(
                                ToolsLang::translateString(
                                    'API FileGetContents has Problems with SSL in %s (%s), second try without peer verify',
                                    'TRWTOOLSPLUGIN'
                                ),
                                $sAPIUrl,
                                $aResult['error']
                            )
                        ]);
                    }

                    $aContext['ssl'] = [
                        "verify_peer"      => false,
                        "verify_peer_name" => false
                    ];
                    $aResult = self::_fileGetContentsWithOptions($sAPIUrl, $aContextOptions);

                    if (isset($aResult['error'])) {
                        if ($bDebug) {
                            self::setDebugLogEntry([
                                'ApiErrorFileGetContentsPeer' => sprintf(
                                    ToolsLang::translateString(
                                        'API FileGetContents has Problems with SSL and peer verify in %s (%s)',
                                        'TRWTOOLSPLUGIN'
                                    ),
                                    $sAPIUrl,
                                    $aResult['error']
                                )
                            ]);
                        }
                    }
                }
            }

            if (isset($aResult['result']) && !isset($aResult['error'])) {
                $mResult = $aResult['result'];
            }

            // possible debugging
            if ($bDebug && !$sTargetFile) {
                self::setDebugLogEntry([
                    'ApiErrorFileGetContentsResult' => $mResult
                ]);
            }

            if ($sTargetFile) {
                file_put_contents($sTargetFile, $mResult);
                $mResult = true;
            }
        }
        return $mResult;
    }

    /**
    * Connect to Webservice via SOAP
    *
    * @param string $sAPIUrl      - The Url of the extern SOAP API
    * @param string $sAPIMethod   - The Method of the extern SOAP API
    * @param string $aAPIOptions  - Array with options for initialize the extern SOAP API
    * @param string $aRequestData - Array with Request-Parameters
    * @param string $bWSDLMode    - Call in WSDLMode?
    * @param string $bDebug       - DebugModus?
    *
    * @return mixed obj or boolean
    */
    public static function executeExternApiSoap($sAPIUrl = '', $sAPIMethod = '', $aAPIOptions = [], $aRequestData = [], $bWSDLMode = false, $bDebug = false)
    {
        $mResult = false;

        if ($sAPIUrl && $sAPIMethod && is_array($aRequestData)) {
            if ($bDebug) {
                self::setDebugLogEntry([
                    'ApiSoapRequestData' => $aRequestData
                ]);
            }

            try {
                $aClientOptions = [
                    'trace'     => 1,
                    'exception' => 0
                ];
                if (is_array($aAPIOptions) && count($aAPIOptions)) {
                    $aClientOptions = array_merge($aClientOptions, $aAPIOptions);
                }
                $oSoapClient = new \SoapClient($sAPIUrl, $aClientOptions);

                if ($bWSDLMode) {
                    $mResult = call_user_func([$oSoapClient, $sAPIMethod], $aRequestData);
                } else {
                    $mResult = $oSoapClient->__soapCall($sAPIMethod, $aRequestData);
                }
            } catch (\SoapFault $e) {
                // possible debugging
                if ($bDebug) {
                    self::setDebugLogEntry([
                        'ApiSoapRequest'        => $oSoapClient->__getLastRequest(),
                        'ApiSoapResponseheader' => $oSoapClient->__getLastResponseHeaders(),
                        'ApiSoapResponse'       => $oSoapClient->__getLastResponse(),
                        'ApiSoapErrormessage'   => $e->getMessage()
                    ]);
                }
            }
        }
        return $mResult;
    }

    /**
    * get the debug Log
    *
    * @param boolean $bTargetTRWToolsLog - should send the Log to the TRWTools
    *
    * @return mixed (array / false)
    */
    public static function getDebugLog($bTargetTRWToolsLog = false)
    {
        $mResult = false;

        if ($bTargetTRWToolsLog) {
            if (count(self::$_aDebugLog)) {
                $aTmp = [];
                foreach (self::$_aDebugLog as $mLogEntryKey => $mLogEntryValue) {
                    // convert object to array
                    if (is_object($mLogEntryValue) || is_array($mLogEntryValue)) {
                        $aTmp[$mLogEntryKey] = print_r($mLogEntryValue, true);
                    } else {
                        $aTmp[$mLogEntryKey] = $mLogEntryKey . ' : ' . $mLogEntryValue;
                    }
                }

                $mResult = ToolsLog::setLogEntries($aTmp, __CLASS__);
            }
        } else {
            $mResult = self::$_aDebugLog;
        }
        return $mResult;
    }

    /**
    * set the attempts for curl Requests
    *
    * @param integer iAttempt
    *
    * @return null
    */
    public static function setCurlAttempts($iAttempt)
    {
        self::$_iCurlAttempts = (int)$iAttempt;
    }

    /**
    * set a debug Log Entry
    *
    * @param mixed $mLogEntry - Log entry (string, array, obj ...)
    *
    * @return null
    */
    public static function setDebugLogEntry($mLogEntry = null)
    {
        if ($mLogEntry) {
            self::$_aDebugLog[] = $mLogEntry;
        }
    }

    /**
    * Connect to Webservice via simple file_get_contents
    *
    * @param string $sAPIUrl  - The Url of the extern API
    * @param array  $aContext - Context-Options for file_get_contents
    *
    * @return array
    */
    private static function _fileGetContentsWithOptions($sAPIUrl, $aContext = null)
    {
        $aResult = [];

        $sContext = null;
        if (is_array($aContext) && count($aContext)) {
            $sContext = stream_context_create($aContext);
        }

        try {
            // file_get_contents can trigger WARNINGS as a valid result,
            // We also want to intercept the warning and give it back as an error
            set_error_handler(
                function ($sErrNo, $sErrStr) {
                    $aResult['error'] = $sErrStr;
                },
                E_WARNING
            );

            $aResult['result'] = file_get_contents($sAPIUrl, false, $sContext);

            restore_error_handler();
        } catch (\Exception $e) {
            $aResult['error'] = $e->getMessage();
        }

        return $aResult;
    }
}
