<?php

/**
* @author    Mario Lorenz, www.the-real-world.de
* @copyright 2021 the-real-world.de
* @license   https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

namespace TheRealWorld\ToolsPlugin\Core;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;
use Isbn\Isbn;
use Html2Text\Html2Text;

class ToolsString
{
    /**
    * array with problematic Utf8-encoded chars in Iso-encoding
    * @param array
    */
    protected static $_aProblemEnc = [
        'euro'    => 'EURO',
        'hellip'  => "...",
        'dagger'  => "+",
        'permil'  => "%o",
        'sbquo'   => "'",
        'lsaquo'  => "'",
        'lsquo'   => "'",
        'rsquo'   => "'",
        'bdquo'   => '"',
        'ldquo'   => '"',
        'rdquo'   => '"',
        'raquo'   => '"',
        'laquo'   => '"',
        'raquo'   => '"',
        'laquo'   => '"',
        'bull'    => "*",
        'middot'  => "*",
        'ndash'   => "-",
        'mdash'   => "-",
        'trade'   => " TM",
        'rsquo'   => "'",
        'brvbar'  => "|",
        'copy'    => " (c)",
        'reg'     => " (r)",
        'plusmn'  => "+/-",
        'micro'   => "micro",
        'para'    => "",
        'nbsp'    => " ",
        'deg'     => ""
    ];

    /**
    * convert a string to ascii, needed for email-creating
    *
    * @param string $sString - the to be converted string
    * @param array  $aReplace - special replaces Array
    * @param boolean $bForEMailAdresses - if it set, only a-z, 0-9, /_|+ - are converted to $sDelimiter
    * @param string $sDelimiter - the central delimiter instead of different delimiters
    *
    * @return string
    */
    public static function convertTextToAscii($sString = '', $aReplace = [], $bForEMailAdresses = true, $sDelimiter = '_', $sUtf8RegionCode = 'de_DE')
    {
        $oStr = Str::getStr();

        setlocale(LC_ALL, $sUtf8RegionCode . '.UTF8');
        if (!empty($aReplace)) {
            $sString = str_replace((array)$aReplace, ' ', $sString);
        }
        $sClean = iconv('UTF-8', 'ASCII//TRANSLIT', $sString);
        if ($bForEMailAdresses) {
            $sClean = $oStr->preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $sClean);
            $sClean = $oStr->strtolower(trim($sClean, '-'));
            $sClean = $oStr->preg_replace("/[\/_|+ -]+/", $sDelimiter, $sClean);
        } else {
            $sClean = $oStr->preg_replace("/[^\x01-\x7F]/", '', $sClean);
        }
        return $sClean;
    }

    /**
    * convert a string to normal chars, all special chars are delete
    *
    * @param string $sString - the to be converted string
    * @param string $sDelimiter - the central delimiter instead of different delimiters
    *
    * @return string
    */
    public static function convertTextToNormalChars($sString, $sDelimiter = ' ', $sExcludePregReplace = '', $bstringToLower = false)
    {
        $oStr = Str::getStr();

        $sResult = $sString;
        $sResult = self::convertHtmlToText($sResult);
        $sResult = $oStr->preg_replace('/[^\p{L}\p{N} ' . $sExcludePregReplace . ']/u', " ", $sResult);
        $sResult = self::deleteManyWhitespaces($sResult);
        $sResult = str_replace(" ", $sDelimiter, $sResult);
        if ($bstringToLower) {
            $sResult = $oStr->strtolower($sResult);
        }
        return $sResult;
    }

    /**
    * convert a Html-String to Text
    *
    * @param string $sString
    *
    * @return string
    */
    public static function convertHtmlToText($sString = '')
    {
        $oHtml2Text = new Html2Text($sString);
        return $oHtml2Text->getText();
    }

    /**
    * clean a ISBN and convert a ISBN10 to ISBN13 if it is possible
    *
    * @param string  $sISBN - ISBN
    * @param boolean $bWithHyphen - add Hyphens
    *
    * @return mixed (boolean / string)
    */
    public static function cleanISBN($sISBN = '', $bWithHyphen = false)
    {
        $mResult = false;
        $oISBN = new Isbn();
        if ($sISBN && $oISBN->check->identify($sISBN) !== false) {
            $sISBN = $oISBN->hyphens->removeHyphens($sISBN);
            if ($oISBN->check->is10($sISBN) && $oISBN->validation->isbn10($sISBN)) {
                $sISBN = $oISBN->translate->to13($sISBN);
            } elseif (!($oISBN->check->is13($sISBN) && $oISBN->validation->isbn13($sISBN))) {
                $sISBN = false;
            }

            if ($bWithHyphen && $sISBN) {
                $sISBN = $oISBN->hyphens->addHyphens($sISBN);
            }

            $mResult = $sISBN;
        }
        return $mResult;
    }

    /**
    * delete Many Whitespaces to only one
    *
    * @param string $sString
    * @param boolean $bAlsoHtml - also delete html whitespaces &nbsp;
    *
    * @return string
    */
    public static function deleteManyWhitespaces($sString = '', $bAlsoHtml = false)
    {
        if ($bAlsoHtml) {
            $sString = str_replace('&nbsp;', ' ', $sString);
        }

        $sString = Str::getStr()->preg_replace(
            ["/[\s\t\n\r\s]+/", "/\s([?.!])/"],
            [" ","$1"],
            $sString
        );

        $sString = str_replace("\xc2\xa0", " ", $sString);

        return $sString;
    }

    /**
    * is the string a valid eMail?
    *
    * @return boolean
    */
    public static function isValidEmail($sMail = '')
    {
        return oxNew(\OxidEsales\Eshop\Core\MailValidator::class)->isValidEmail($sMail);
    }

    /**
    * is the string a valid MySQL Date?
    *
    * @return boolean
    */
    public static function isValidMySqlDate($sDate = '')
    {
        if (Str::getStr()->preg_match("/^(\d{4})-(\d{2})-(\d{2}) ([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/", $sDate, $matches)) {
            if (checkdate($matches[2], $matches[3], $matches[1])) {
                return true;
            }
        }
        return false;
    }

    /**
    * check if a string is utf-8 encoded
    *
    * @param string $sString - the to be checked string
    *
    * @return string
    */
    public static function isUtf8($sString = '')
    {
        return (mb_detect_encoding($sString . ' ', 'UTF-8', true) === 'UTF-8');
    }

    /**
    * convert a Text-String to Utf8
    *
    * @param string $sString
    *
    * @return string
    */
    public static function encodeUtf8($sString = '', $sBaseEncoding = "ISO-8859-15")
    {
        if (!self::isUtf8($sString)) {
            $sString = mb_convert_encoding($sString, "UTF-8", $sBaseEncoding);
            //$sString = utf8_encode($sString);
        }

        return $sString;
    }

    /**
    * split a String on the last occurrence of a splitter
    *
    * @param string $sString - string to split
    * @param mixed $mSplitter - splitter - string or array
    * @param boolean $bReturnAfter - return the Rest after the splitting
    *
    * @return string
    */
    public static function splitOnLastOccurrence($sString = '', $mSplitter = ' ', $bReturnAfter = true)
    {
        $sResult = $sString;

        if (!is_array($mSplitter)) {
            $mSplitter = [$mSplitter];
        }

        if ($sString) {
            foreach ($mSplitter as $sSplitter) {
                if ($iFoundSplitter = strrpos($sString, $sSplitter)) {
                    if ($bReturnAfter) {
                        $sResult = trim(substr($sString, $iFoundSplitter + Str::getStr()->strlen($sSplitter)));
                    } else {
                        $sResult = trim(substr($sString, 0, $iFoundSplitter));
                    }
                    break;
                }
            }
        }
        return $sResult;
    }

    /**
    * format a platform (like Google) conform price with a decimal "point"
    *
    * @param string $sString
    *
    * @return string
    */
    public static function formatPlatformPrice($sString)
    {
        $oCurrency = Registry::getConfig()->getActShopCurrencyObject();
        $sString = number_format((double)$sString, $oCurrency->decimal, '.', '') . ' ' . $oCurrency->name;
        return $sString;
    }

    /**
    * enhanced UTF8-Decoder
    *
    * @param string $sString - string to decode
    *
    * @return string
    */
    public static function decodeUtf8($sString = '')
    {
        if (self::isUtf8($sString)) {
            //Problem is that utf8_decode convert HTML chars for &bdquo; and other to ? or &nbsp;
            // to \xA0. And you can not replace, that they are in some char bytes and you broke
            // cyrillic (or other alphabet) chars.
            $sString = mb_convert_encoding($sString, 'HTML-ENTITIES', 'UTF-8');
            $sString = Str::getStr()->preg_replace_callback(
                '#(?<!\&ETH;)\&(' . implode('|', array_keys(self::$_aProblemEnc)) . ');#s',
                "self::_callBackProblemEnc",
                $sString
            );
            $sString = mb_convert_encoding($sString, 'UTF-8', 'HTML-ENTITIES');
            $sString = utf8_decode($sString);
        }
        return $sString;
    }

    /**
    * Checks if a value exists in an array case insensitive
    *
    * @param string $sNeedle - The searched value
    * @param string $aHaystack - The array
    *
    * @return string
    */
    public static function inArrayI($sNeedle, $aHaystack)
    {
        return in_array(Str::getStr()->strtolower($sNeedle), array_map('strtolower', $aHaystack));
    }

    /**
    * stripTags method with blacklist
    *
    * @param string sString
    * @param array $aBlacklist - array with htmlelements that should remove,
    *                            the elements without lace braces
    *                            e.g. $aBlacklist = ['br', 'p', 'div']
    * @return string
    */
    public static function stripTagsWithBlacklist($sString, $aBlacklist = null)
    {
        if (is_array($aBlacklist)) {
            $sBlacklist = implode('|', $aBlacklist);
            $sString = preg_replace(
                '/<\/?(' . $sBlacklist . ')\/?>/m',
                '',
                $sString
            );
        }
        return $sString;
    }

    /**
    * callBack function for preg_replace_callback in above method decodeUtf8
    *
    * @param array - $aMatches
    *
    * @return string
    */
    protected static function _callBackProblemEnc($aMatches)
    {
        return self::$_aProblemEnc[$aMatches[1]];
    }
}
