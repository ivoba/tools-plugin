<?php

/**
* @author    Mario Lorenz, www.the-real-world.de
* @copyright 2021 the-real-world.de
* @license   https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

namespace TheRealWorld\ToolsPlugin\Core;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class ToolsMonologLogger
{
    /**
    * Logger
    * @param array with Logger Obj
    */
    protected static $_aLogger = [];

    /**
    * get Logger
    *
    * @return Monolog\Logger
    */
    public static function getLogger(string $sLogger = '', string $sPath = '', string $sLogLevel = '')
    {
        $sLogger = (!empty($sLogger) ? $sLogger : 'OXID Logger');

        if (!array_key_exists($sLogger, self::$_aLogger)) {
            $sPath = (!empty($sPath) ? $sPath : Registry::getConfig()->getLogsDir() . 'oxideshop.log');
            if (!is_file($sPath)) {
                file_put_contents($sPath, '');
            }

            $sLogLevel = Str::getStr()->strtoupper($sLogLevel);
            $sLogLevel = ((!empty($sLogLevel) && defined("Logger::$sLogLevel")) ? constant("Logger::$sLogLevel") : Logger::DEBUG);

            self::$_aLogger[$sLogger] = new Logger($sLogger);
            self::$_aLogger[$sLogger]->pushHandler(new StreamHandler(
                $sPath,
                $sLogLevel
            ));
        }

        return self::$_aLogger[$sLogger];
    }
}
