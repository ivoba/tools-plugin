<?php

/**
* @author    Mario Lorenz, www.the-real-world.de
* @copyright 2021 the-real-world.de
* @license   https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

namespace TheRealWorld\ToolsPlugin\Core;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;
use TheRealWorld\ToolsPlugin\Core\ToolsString;

class ToolsLang
{
    /**
    * try to translate a string
    *
    * @param string sString - string to translate
    * @param string sPrefix - possible prefix for language variables.
    *
    * @return string
    */
    public static function translateString($sString = '', $sPrefix = '')
    {
        $oLang = Registry::getLang();
        $sTranlateString = str_replace('%s', '', $sString);
        $sTranlateString = trim(preg_replace('/[^a-zA-Z]/', ' ', $sTranlateString));
        $sTranlateString = ToolsString::deleteManyWhitespaces($sTranlateString);
        $sTranlateString = str_replace(' ', '_', $sTranlateString);
        $sTranlateString = $sPrefix . '_' . Str::getStr()->strtoupper($sTranlateString);
        $sTranlateString = $oLang->translateString($sTranlateString);
        if ($oLang->isTranslated()) {
            $sString = $sTranlateString;
        }
        return $sString;
    }
}
