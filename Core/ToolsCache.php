<?php

/**
* @author    Mario Lorenz, www.the-real-world.de
* @copyright 2021 the-real-world.de
* @license   https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

namespace TheRealWorld\ToolsPlugin\Core;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;
use OxidEsales\Eshop\Core\DatabaseProvider;
use TheRealWorld\ToolsPlugin\Core\ToolsFile;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;
use TheRealWorld\ToolsPlugin\Core\ToolsConfig;

class ToolsCache
{
    /**
    * the cache clean
    *
    * @return boolean
    */
    public static function cleanCache()
    {
        $oStr = Str::getStr();
        // TMP-Files
        $sTmpDir = Registry::getConfig()->getConfigParam('sCompileDir') . "/";
        $sTmpDir = $oStr->preg_replace("/(\/)+/", "/", $sTmpDir);
        if (is_dir($sTmpDir)) {
            ToolsFile::deleteFilesFromPath($sTmpDir);
        }

        // Smarty-Files
        $sSmartyDir = Registry::getUtilsView()->getSmartyDir() . "/";
        $sSmartyDir = $oStr->preg_replace("/(\/)+/", "/", $sSmartyDir);
        if (is_dir($sSmartyDir)) {
            ToolsFile::deleteFilesFromPath($sSmartyDir);
        }

        return true;
    }

    /**
    * the image reseter
    *
    * @return boolean
    */
    public static function resetImages()
    {
        ToolsFile::deleteRecursively(
            Registry::getConfig()->getPicturePath(null) . 'generated/',
            false
        );
        return true;
    }

    /**
    * the update views
    *
    * @return boolean
    */
    public static function updateViews()
    {
        $oMetaData = oxNew(\OxidEsales\Eshop\Core\DbMetaDataHandler::class);
        return $oMetaData->updateViews();
    }

    /**
    * clean dynamic Seo-Urls
    *
    * @return boolean
    */
    public static function cleanDynamicSeoUrls()
    {
        $sSql = "delete from `oxseo` where `oxtype` != 'static'";
        return (ToolsDB::execute($sSql) !== false);
    }

    /**
    * reset tpl blocks
    *
    * @return boolean
    */
    public static function resetTplBlocks()
    {
        foreach(Registry::getConfig()->getShopIds() as $iShopId) {

            $sSql = "delete
                from `oxtplblocks`
                where oxid in
                (
                    select min(e.`oxid`)
                    from (
                        select * from `oxtplblocks`
                        where `oxshopid` = " . $iShopId . "
                    ) e
                    group by `oxmodule`, `oxtheme`, `oxfile`, `oxblockname`, `oxtemplate`, `oxpos`, `oxshopid`, `oxactive`
                    having count(*) > 1
                )";
            ToolsDB::execute($sSql);
        }
        return true;
    }

    /**
    * cleanup the Modules
    *
    * @return boolean
    */
    public static function cleanUpModules()
    {
        $oConfig = Registry::getConfig();
        $oModuleList = oxNew(\OxidEsales\Eshop\Core\Module\ModuleList::class);

        // OXID Standard CleanUp
        $oModuleList->cleanup();

        // collect ModuleOptionsKeys
        $aModuleOptionsKeys = [
            'aModule' . $oModuleList::MODULE_KEY_PATHS,
            'aModule' . $oModuleList::MODULE_KEY_EVENTS,
            'aModule' . $oModuleList::MODULE_KEY_VERSIONS,
            'aModule' . $oModuleList::MODULE_KEY_EXTENSIONS,
            'aModule' . $oModuleList::MODULE_KEY_FILES,
            'aModule' . $oModuleList::MODULE_KEY_TEMPLATES,
            'aModule' . $oModuleList::MODULE_KEY_CONTROLLERS,
            'aDisabledModules',
            'moduleSmartyPluginDirectories'
        ];

        // search for really exists Modules
        $aModulesInConfig = array_merge(
            $oModuleList->getDisabledModuleInfo(),
            $oModuleList->getActiveModuleInfo()
        );

        $aReallyExistsModules = [];
        foreach ($aModulesInConfig as $sModuleId => $sModulePath) {
            if (is_dir($oConfig->getModulesDir() . $sModulePath)) {
                $aReallyExistsModules[$sModuleId] = $sModulePath;
            }
        }


        foreach($oConfig->getShopIds() as $iShopId) {

            // cleanUp ModuleOptions
            foreach ($aModuleOptionsKeys as $sKey) {
                $aModuleOption = $oConfig->getShopConfVar($sKey, $iShopId, '');
                $aNewModuleOption = [];
                foreach ($aModuleOption as $sModuleId => $aOptions) {
                    if ($sModuleId && array_key_exists($sModuleId, $aReallyExistsModules)) {
                        $aNewModuleOption[$sModuleId] = $aOptions;
                    }
                }
                ToolsConfig::saveConfigParam($sKey, $aNewModuleOption, 'aarr', '', $iShopId);
            }

            // remove extended not exists classes
            $aNotExistsModules = array_diff($aModulesInConfig, $aReallyExistsModules);

            if (count($aNotExistsModules)) {
                $aExtensionsToDelete = [];
                foreach ($aNotExistsModules as $sModuleId) {
                    $aExtensionsToDelete = array_merge_recursive(
                        $aExtensionsToDelete,
                        $oModuleList->getModuleExtensions($sModuleId)
                    );
                }

                $aUpdatedExtensions = $oModuleList->diffModuleArrays($aModuleExtensions, $aExtensionsToDelete);
                $aUpdatedExtensionsChains = $oModuleList->buildModuleChains($aUpdatedExtensions);
                ToolsConfig::saveConfigParam('aModules', $aUpdatedExtensionsChains, 'aarr');
            }

            // remove module-options from not exists Modules
            $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
            $sSql = "select `oxmodule`
                from `oxconfig`
                where `oxmodule` like 'module:%'
                and `oxshopid` = " . $iShopId . "
                group by oxmodule";
            $aModulesInConfig = $oDb->getAll($sSql);

            foreach ($aModulesInConfig as $aModuleInConfig) {
                $sModuleId = substr_replace($aModuleInConfig['oxmodule'], '', 0, strlen('module:'));
                if (!array_key_exists($sModuleId, $aReallyExistsModules)) {
                    $sSql = "delete from `oxconfig`
                        where `oxmodule` = " . $oDb->quote('module:' . $sModuleId) . "
                        and `oxshopid` = " . $iShopId;
                    ToolsDB::execute($sSql);

                    $sSql = "delete from `oxconfigdisplay`
                        where `oxcfgmodule`= " . $oDb->quote('module:' . $sModuleId);
                    ToolsDB::execute($sSql);
                }
            }
        }

        // Remove from caches.
        \OxidEsales\Eshop\Core\Module\ModuleVariablesLocator::resetModuleVariables();

        return true;
    }
}
